/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef Q3DSIMGUIITEM_H
#define Q3DSIMGUIITEM_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of a number of Qt sources files.  This header file may change from
// version to version without notice, or even be removed.
//
// We mean it.
//

#include <Qt3DStudioRuntime2/private/q3dsruntimeglobal_p.h>
#include <QQuickItem>
#include <QImage>
#include <QSGRenderNode>

struct ImGuiContext;

QT_BEGIN_NAMESPACE

class QOpenGLTexture;
class QOpenGLShaderProgram;
class QOpenGLBuffer;
class QOpenGLVertexArrayObject;
class Q3DSQuickImGuiInputEventFilter;

class Q3DSQuickImGuiRenderer : public QSGRenderNode
{
public:
    Q3DSQuickImGuiRenderer();
    ~Q3DSQuickImGuiRenderer();

    void render(const RenderState *state) override;
    void releaseResources() override;
    StateFlags changedStates() const override;
    RenderingFlags flags() const override;
    QRectF rect() const override;

    struct FrameDesc {
        QVector<QImage> textures;
        struct Cmd {
            uint elemCount;
            const void *indexOffset;
            QPointF scissorPixelBottomLeft;
            QSizeF scissorPixelSize;
            uint textureIndex;
        };
        struct CmdListEntry {
            QByteArray vbuf;
            QByteArray ibuf;
            QVector<Cmd> cmds;
        };
        QVector<CmdListEntry> cmdList;
    };

private:
    QPointF m_scenePixelPosBottomLeft;
    QSizeF m_itemPixelSize;
    QSizeF m_itemSize;
    float m_dpr;
    FrameDesc m_frameDesc;
    QVector<QOpenGLTexture *> m_textures;
    QOpenGLShaderProgram *m_program = nullptr;
    int m_mvpLoc;
    int m_texLoc;
    int m_opacityLoc;
    QOpenGLVertexArrayObject *m_vao = nullptr;
    QOpenGLBuffer *m_vbo = nullptr;
    QOpenGLBuffer *m_ibo = nullptr;

    friend class Q3DSImGuiItem;
};

class Q3DSV_PRIVATE_EXPORT Q3DSImGuiItem : public QQuickItem
{
    Q_OBJECT

public:
    Q3DSImGuiItem(QQuickItem *parent = nullptr);
    ~Q3DSImGuiItem();

signals:
    void frame();

private:
    QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *) override;
    void itemChange(ItemChange, const ItemChangeData &) override;
    void updatePolish() override;

    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
#if QT_CONFIG(wheelevent)
    void wheelEvent(QWheelEvent *event) override;
#endif
    void hoverMoveEvent(QHoverEvent *event) override;
    void touchEvent(QTouchEvent *event) override;

    void initialize();
    void cleanup();
    void updateTheme();
    void setInputEventSource(QObject *src);
    void updateInput();

    QQuickWindow *m_w = nullptr;
    qreal m_dpr;
    QMetaObject::Connection m_c;
    ImGuiContext *m_imGuiContext = nullptr;
    Q3DSQuickImGuiRenderer::FrameDesc m_frameDesc;
    bool m_inputInitialized = false;
    Q3DSQuickImGuiInputEventFilter *m_inputEventFilter = nullptr;
    QObject *m_inputEventSource = nullptr;
    QObject m_dummy;
};

QT_END_NAMESPACE

#endif
