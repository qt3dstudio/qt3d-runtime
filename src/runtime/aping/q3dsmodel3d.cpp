/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dsmodel3d_p.h"

QT_BEGIN_NAMESPACE

Q3DSModel3D::Q3DSModel3D()
{

}

void Q3DSModel3D::setMesh(const QString &meshUrl)
{
    if (m_mesh == meshUrl)
        return;

    m_mesh = meshUrl;
    emit meshChanged(m_mesh);
}

Q3DSGraphObject *Q3DSModel3D::createObject(Q3DSUipPresentation *presentation)
{
    // id is always unique, name does not have to be. We will use the same unique string for both.
    const QByteArray id = makeIdAndName();

    // TODO: This is just for toying around, we need a proper solution for adding e.g., materials.
    Q3DSModelNode *model = presentation->newObject<Q3DSModelNode>(id);
    model->setName(QString::fromLatin1(id));
    model->setMesh(!m_mesh.isEmpty() ? m_mesh : QString::fromLatin1("#Rectangle"));

    const QByteArray matId = QByteArray("_material-") + id;
    Q3DSDefaultMaterial *defMat = presentation->newObject<Q3DSDefaultMaterial>(matId);
    defMat->setName(QString::fromLatin1(matId));
    defMat->setDiffuse(QColor(Qt::green));
    model->appendChildNode(defMat);

    return model;
}

QT_END_NAMESPACE
