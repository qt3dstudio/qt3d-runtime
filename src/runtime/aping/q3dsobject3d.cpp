/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dsobject3d_p.h"
#include "q3dslayer3d_p.h"

QT_BEGIN_NAMESPACE

/*!
    \qmltype Object3D
    //! \instantiates Q3DSObject3D
    \inqmlmodule QtStudio3D
    \ingroup 3dstudioruntime2
    \inherits Object
    \since Qt 3D Studio 2.1

    \internal
 */

class Q3DSPropertyBinder : public QObject
{
public:
    ~Q3DSPropertyBinder() { unbind(); }
    static QByteArray binderName() { return QByteArrayLiteral("_q3dsbinding"); }

    Q3DSPropertyBinder(QObject *p = nullptr) : QObject(p) {}
    int qt_metacall(QMetaObject::Call call, int methodId, void **args) override
    {
        if (methodId < 0)
            return methodId;

        const quint8 dstId = (methodId >> 8) & 0xff;
        methodId = QObject::qt_metacall(call, (methodId & 0xff), args);
        if (methodId < 0)
            return methodId;

        if (!m_target || !m_src)
            return  methodId;

        if (call == QMetaObject::InvokeMetaMethod) {
            Q_ASSERT(methodId > -1);
            const QMetaProperty &p = m_src->metaObject()->property(methodId);
            const QMetaProperty &tp = m_target->metaObject()->property(dstId);
            if (!m_src->propertyChanged(p, m_target, tp))
                propertyChanged(p, tp);
            return -1;
        }
        return methodId;
    }

    void bind(Q3DSObject3D *src, Q3DSGraphObject *target)
    {
        if (src == m_src && target == m_target)
            return;

        unbind();

        m_src = src;
        m_src->setProperty(binderName(), QVariant::fromValue(this));
        m_target = target;
        m_target->setProperty(binderName(), QVariant::fromValue(this));
        const int propCount = src->metaObject()->propertyCount();
        for (int i = 0; i != propCount; ++i) {
            const auto property = src->metaObject()->property(i);
            if (!property.hasNotifySignal())
                continue;
            QMetaProperty props[2];
            for (int k = 0; k != target->metaObject()->propertyCount(); ++k) {
                const auto &prop = target->metaObject()->property(k);
                if (prop.type() != property.type())
                    continue;
                const struct data { int idx; int offset; } d = (::strncmp("qq_", prop.name(), 3) == 0) ? data{ 0, 3 } : data{ 1, 0 };
                if (::strcmp(prop.name() + d.offset, property.name()) == 0)
                    props[d.idx] = prop;

                if (props[0].isValid())
                    break;
            }
            const auto &tp = props[0].isValid() ? props[0] : props[1];
            const int targetIdx = tp.propertyIndex();
            if (targetIdx < 0)
                continue;
            const int srcIdx = (i + metaObject()->methodCount());
            if (srcIdx > 0xff || targetIdx > 0xff) {
                qWarning("Unable to bind property %s from %s to %s",
                         property.name(), qPrintable(m_src->objectName()), qPrintable(m_target->name()));
                continue;
            }
            // Sync
            if (!m_src->propertyChanged(property, m_target, tp))
                propertyChanged(property, tp);
            QMetaObject::connect(src, property.notifySignalIndex(), this, (targetIdx << 8) | srcIdx, Qt::DirectConnection, nullptr);
        }
    }

    void unbind()
    {
        if (m_src) {
            const QVariant h = m_src->property(binderName());
            Q_ASSERT(this == h.value<Q3DSPropertyBinder *>());
            m_src->setProperty(binderName(), QVariant());
        }

        if (m_target) {
            const QVariant h = m_target->property(binderName());
            Q_ASSERT(this == h.value<Q3DSPropertyBinder *>());
            m_target->setProperty(binderName(), QVariant());
        }

        disconnect();
        m_src = nullptr;
        m_target = nullptr;
    }

private:
    void propertyChanged(const QMetaProperty &sp, const QMetaProperty &tp)
    {
        const QVariant &v = sp.read(m_src);
        if (v.isValid() && !tp.writeOnGadget(m_target, v)) {
            qWarning("Failed to write property %s on %s", sp.name(), m_target->id().constData());
            return;
        }

        m_target->notifyPropertyChanges({QString::fromLatin1(sp.name()), QString()});
    }

    Q3DSObject3D *m_src = nullptr;
    Q3DSGraphObject *m_target = nullptr;
};

// No parent parameter here. QML does not need it and not offering it helps
// autotests avoiding the trap of overridden-virtuals-not-called-from-ctor
// problems (like that we lose the ItemParentHasChanged because our
// itemChange() is not getting called)
Q3DSObject3D::Q3DSObject3D()
{
    setFlag(ItemHasContents, false);
}

Q3DSObject3D::~Q3DSObject3D()
{
    // When the object is not attached to a parent, we own it,
    // otherwise the parent takes care of destroying it.
    if (m_object && !m_object->parent())
        delete m_object;
}

// Deferred fun: Creating the underlying Q3DSGraphObject is not posssible
// without the presentation. The presentation comes from the engine via a
// Layer3D which we do not know until we or an ancestor gets parented to one.

void Q3DSObject3D::itemChange(ItemChange change, const ItemChangeData &changeData)
{
    switch (change) {
    case ItemParentHasChanged:
        if (m_object && m_object->parent()) {
            if (m_layer) {
                Q3DSUipPresentation::forAllObjectsInSubTree(m_object, [this](Q3DSGraphObject *obj) {
                    m_layer->slide()->removeObject(obj);
                });
            }
            m_object->parent()->removeChildNode(m_object);
            if (m_layer)
                invalidate(InvalidateLayer);
        }
        if (changeData.item) {
            // cancel outstanding operations
            m_pendingObjectParent = nullptr;
            if (m_hasPendingActivate) {
                // in case our parent changes twice before the Layer3D gets added to a window
                m_hasPendingActivate = false;
                disconnect(m_layerConn);
            }
            // see if we are parented to a Layer3D or an Object3D
            Q3DSLayer3D *layer3D = qobject_cast<Q3DSLayer3D *>(changeData.item);
            if (layer3D) {
                // if the layer has a backing Q3DSLayerNode and is attached to
                // a Studio3DEngine then we can proceed right away
                if (layer3D->isLive()) {
                    m_layer = layer3D;
                    activate();
                } else {
                    // defer otherwise
                    m_hasPendingActivate = true;
                    m_layerConn = connect(layer3D, &Q3DSLayer3D::layerNodeCreated, this, [this, layer3D] {
                        m_layer = layer3D;
                        activate();
                    });
                }
            } else {
                Q3DSObject3D *obj3D = qobject_cast<Q3DSObject3D *>(changeData.item);
                if (obj3D) {
                    // if the backing Q3DSGraphObject is present, check if the
                    // same is true for the new parent and if it is associated
                    // with a Layer3D that is attached to a Studio3DEngine
                    // since things are simple then
                    if (m_object && obj3D->object() && obj3D->layer() && obj3D->layer()->isLive()) {
                        // there will not be an activate() so do everything needed right here
                        setLayer_recursive(obj3D->layer());
                        Q3DSUipPresentation::forAllObjectsInSubTree(m_object, [this](Q3DSGraphObject *obj) {
                            m_layer->slide()->addObject(obj);
                        });
                        obj3D->object()->appendChildNode(m_object);
                        m_pendingObjectParent = nullptr;
                    } else {
                        // defer otherwise, there will be an activate() eventually (or not but that's fine too)
                        m_pendingObjectParent = obj3D;
                    }
                }
            }
        }
        break;

    default:
        break;
    }
}

void Q3DSObject3D::activate_helper(Q3DSLayer3D *layer)
{
    m_layer = layer;

    // m_object may or may not be null.
    //
    // When this Object3D has never been associated with a subtree belonging to
    // a Layer3D attached to a window, then m_object is still null.
    //
    // Otherwise m_object is valid, that is the case where a fully initialized
    // Object3D with an existing underlying Q3DSGraphObject gets moved out from
    // under a Layer3D, and then later gets added to another one.

    if (!m_object) {
        m_object = createObject(m_layer->presentation());
        if (!m_object)
            return;
    }

    m_layer->slide()->addObject(m_object);

    if (m_pendingObjectParent) {
        Q_ASSERT(m_pendingObjectParent->object());
        m_pendingObjectParent->object()->appendChildNode(m_object);
        m_pendingObjectParent = nullptr;
    }

    for (QQuickItem *item : childItems()) {
       Q3DSObject3D *child = qobject_cast<Q3DSObject3D *>(item);
       if (child)
           child->activate_helper(m_layer);
    }
}

void Q3DSObject3D::activate()
{
    Q_ASSERT(!m_pendingObjectParent); // this must be an Object3D parented to a Layer3D
    Q_ASSERT(m_layer && m_layer->isLive());

    m_hasPendingActivate = false;

    activate_helper(m_layer);
    if (!m_object) // can happen only if the subclass tries a non-unique id, handle it gracefully nonetheless
        return;

    // Connect the object and its children to the Q3DSLayerNode. If that is
    // parented to a scene then the subtree becomes alive now.
    m_layer->layerNode()->appendChildNode(m_object);

    // Sync the properties and make sure they continue being updated.
    Q3DSPropertyBinder *binder = nullptr;
    const QVariant v = property(Q3DSPropertyBinder::binderName());
    if (v.isValid())
        binder = v.value<Q3DSPropertyBinder *>();

    if (!binder)
        binder = new Q3DSPropertyBinder(this);
    binder->bind(this, m_object);
}

void Q3DSObject3D::setLayer_recursive(Q3DSLayer3D *layer)
{
    m_layer = layer;

    for (QQuickItem *item : childItems()) {
       Q3DSObject3D *child = qobject_cast<Q3DSObject3D *>(item);
       if (child)
           child->setLayer_recursive(layer);
    }
}

void Q3DSObject3D::invalidate(InvalidateFlags flags)
{
    if (flags.testFlag(InvalidateLayer))
        m_layer = nullptr;

    if (flags.testFlag(InvalidateObject))
        m_object = nullptr;

    for (QQuickItem *item : childItems()) {
       Q3DSObject3D *child = qobject_cast<Q3DSObject3D *>(item);
       if (child)
           child->invalidate(flags);
    }
}

QByteArray Q3DSObject3D::makeIdAndName() const
{
    QByteArray id = metaObject()->className();
    id += QByteArrayLiteral("_0x");
    id += QByteArray::number(quintptr(this), 16);
    return id;
}

bool Q3DSObject3D::propertyChanged(const QMetaProperty &property,
                                   Q3DSGraphObject *target,
                                   const QMetaProperty &targetProperty)
{
    Q_UNUSED(property);
    Q_UNUSED(target);
    Q_UNUSED(targetProperty);

    return false;
}

QT_END_NAMESPACE
