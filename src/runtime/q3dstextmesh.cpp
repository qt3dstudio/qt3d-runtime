/****************************************************************************
**
** Copyright (C) 2019 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dstextmesh_p.h"

#include <Qt3DRender/qbuffer.h>
#include <Qt3DRender/qattribute.h>

#if QT_VERSION >= QT_VERSION_CHECK(5,12,2)

QT_BEGIN_NAMESPACE

namespace  {
    template <typename T>
    static void fillIndexBuffer(uint quadCount, Qt3DRender::QBuffer *indexBuffer)
    {
        QVector<T> indexes;

        const uint triangleCount = 2 * quadCount;
        indexes.resize(3 * int(triangleCount));

        Q_ASSERT(indexes.size() % 6 == 0);

        for (uint i = 0; i < quadCount; i ++) {
            indexes[int(i * 6 + 0)] = T(i * 4 + 0);
            indexes[int(i * 6 + 1)] = T(i * 4 + 3);
            indexes[int(i * 6 + 2)] = T(i * 4 + 1);

            indexes[int(i * 6 + 3)] = T(i * 4 + 1);
            indexes[int(i * 6 + 4)] = T(i * 4 + 3);
            indexes[int(i * 6 + 5)] = T(i * 4 + 2);
        }

        indexBuffer->setData(QByteArray(reinterpret_cast<const char *>(indexes.constData()),
                                        indexes.size() * int(sizeof(T))));
    }

    class Q3DSTextMeshGeometry : public Qt3DRender::QGeometry
    {
    public:
        Q3DSTextMeshGeometry(bool shadow,
                             const QVector<float> &vertexes,
                             Qt3DCore::QNode *parent = nullptr);
    private:
        Qt3DRender::QBuffer *m_vertexBuffer;
        Qt3DRender::QBuffer *m_indexBuffer;
        Qt3DRender::QAttribute *m_positionAttribute;
        Qt3DRender::QAttribute *m_textureCoordinateAttribute;
        Qt3DRender::QAttribute *m_indexAttribute;
        Qt3DRender::QAttribute *m_textureBoundsAttribute;
    };

    Q3DSTextMeshGeometry::Q3DSTextMeshGeometry(bool shadow,
                                               const QVector<float> &vertexes,
                                               Qt3DCore::QNode *parent)
        : Qt3DRender::QGeometry(parent)
    {
        m_vertexBuffer = new Qt3DRender::QBuffer(this);
        m_vertexBuffer->setUsage(Qt3DRender::QBuffer::StaticDraw);
        m_vertexBuffer->setAccessType(Qt3DRender::QBuffer::Read);
        m_vertexBuffer->setData(QByteArray(reinterpret_cast<const char *>(vertexes.constData()),
                                           int(vertexes.size() * sizeof(float))));

        const uint floatsPerVertex = shadow ? 3 + 2 + 4 : 3 + 2;
        const uint stride = floatsPerVertex * sizeof(float);

        Q_ASSERT(uint(vertexes.size()) % floatsPerVertex == 0);
        const uint vertexCount = uint(vertexes.size()) / floatsPerVertex;

        Q_ASSERT(vertexCount % 4 == 0);
        const uint quadCount = vertexCount / 4;

        m_indexBuffer = new Qt3DRender::QBuffer(this);
        m_indexBuffer->setUsage(Qt3DRender::QBuffer::StaticDraw);
        m_indexBuffer->setAccessType(Qt3DRender::QBuffer::Read);

        Qt3DRender::QAttribute::VertexBaseType indexType;
        if (vertexCount <= 0xffff) {
            fillIndexBuffer<ushort>(quadCount, m_indexBuffer);
            indexType = Qt3DRender::QAttribute::UnsignedShort;
        } else {
            fillIndexBuffer<uint>(quadCount, m_indexBuffer);
            indexType = Qt3DRender::QAttribute::UnsignedInt;
        }

        m_positionAttribute = new Qt3DRender::QAttribute(this);
        m_positionAttribute->setName(QStringLiteral("vCoord"));
        m_positionAttribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
        m_positionAttribute->setVertexSize(3);
        m_positionAttribute->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
        m_positionAttribute->setBuffer(m_vertexBuffer);
        m_positionAttribute->setByteStride(stride);
        m_positionAttribute->setCount(vertexCount);
        setBoundingVolumePositionAttribute(m_positionAttribute);

        m_textureCoordinateAttribute = new Qt3DRender::QAttribute(this);
        m_textureCoordinateAttribute->setName(QStringLiteral("tCoord"));
        m_textureCoordinateAttribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
        m_textureCoordinateAttribute->setVertexSize(2);
        m_textureCoordinateAttribute->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
        m_textureCoordinateAttribute->setBuffer(m_vertexBuffer);
        m_textureCoordinateAttribute->setByteStride(stride);
        m_textureCoordinateAttribute->setByteOffset(3 * sizeof(float));
        m_textureCoordinateAttribute->setCount(vertexCount);

        if (shadow) {
            m_textureBoundsAttribute = new Qt3DRender::QAttribute(this);
            m_textureBoundsAttribute->setName(QStringLiteral("textureBounds"));
            m_textureBoundsAttribute->setVertexBaseType(Qt3DRender::QAttribute::Float);
            m_textureBoundsAttribute->setVertexSize(4);
            m_textureBoundsAttribute->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
            m_textureBoundsAttribute->setBuffer(m_vertexBuffer);
            m_textureBoundsAttribute->setByteStride(stride);
            m_textureBoundsAttribute->setByteOffset(5 * sizeof(float));
            m_textureBoundsAttribute->setCount(vertexCount);
        } else {
            m_textureBoundsAttribute = nullptr;
        }

        m_indexAttribute = new Qt3DRender::QAttribute(this);
        m_indexAttribute->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
        m_indexAttribute->setVertexBaseType(indexType);
        m_indexAttribute->setBuffer(m_indexBuffer);
        m_indexAttribute->setCount(quadCount * 6);

        addAttribute(m_positionAttribute);
        addAttribute(m_textureCoordinateAttribute);
        addAttribute(m_indexAttribute);
        if (shadow)
            addAttribute(m_textureBoundsAttribute);
    }
}

Q3DSTextMesh::Q3DSTextMesh(Qt3DCore::QNode *parent)
    : Qt3DRender::QGeometryRenderer(parent)
    , m_geometry(nullptr)
{
}

void Q3DSTextMesh::setVertexes(const QVector<float> &vertexes, bool dropShadow)
{
    delete m_geometry;
    m_geometry = new Q3DSTextMeshGeometry(dropShadow, vertexes, this);
    setGeometry(m_geometry);
}

Q3DSTextMesh::~Q3DSTextMesh()
{
}

QT_END_NAMESPACE

#endif // QT_VERSION >= QT_VERSION_CHECK(5,12,2)
