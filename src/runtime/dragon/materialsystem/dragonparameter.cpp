/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "dragonparameter_p.h"

#include <Qt3DCore/qpropertyupdatedchange.h>

#include <Qt3DRender/qparameter.h>
#include <Qt3DRender/private/qparameter_p.h>

#include <private/dragonstringtoint_p.h>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

Parameter::Parameter()
    : BackendNode()
    , m_nameId(-1)
{
}

void Parameter::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<QParameterData>>(change);
    const auto &data = typedChange->data;
    m_name = data.name;
    m_nameId = DragonStringToInt::lookupId(m_name);
    m_uniformValue = UniformValue::fromVariant(data.backendValue);
}

void Parameter::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    QPropertyUpdatedChangePtr propertyChange = qSharedPointerCast<QPropertyUpdatedChange>(e);

    if (e->type() == PropertyUpdated) {
        if (propertyChange->propertyName() == QByteArrayLiteral("name")) {
            m_name = propertyChange->value().toString();
            m_nameId = DragonStringToInt::lookupId(m_name);
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("value")) {
            m_uniformValue = UniformValue::fromVariant(propertyChange->value());
            markDirty();
        }
    }

    BackendNode::sceneChangeEvent(e);
}

QString Parameter::name() const
{
    return m_name;
}

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE
