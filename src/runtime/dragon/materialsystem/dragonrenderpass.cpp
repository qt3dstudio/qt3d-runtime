/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "dragonrenderpass_p.h"

#include <Qt3DRender/private/qrenderpass_p.h>

#include <Qt3DCore/qpropertynodeaddedchange.h>
#include <Qt3DCore/qpropertynoderemovedchange.h>
#include <Qt3DCore/qpropertyupdatedchange.h>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

void RenderPass::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<QRenderPassData>>(
        change);
    const auto &data = typedChange->data;
    m_filterKeyList = data.filterKeyIds;
    m_parameterPack.setParameters(data.parameterIds);
    for (const auto &renderStateId : qAsConst(data.renderStateIds))
        addRenderState(renderStateId);
    m_shaderUuid = data.shaderId;
}

void RenderPass::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    switch (e->type()) {
    case PropertyValueAdded: {
        const auto change = qSharedPointerCast<QPropertyNodeAddedChange>(e);
        if (change->propertyName() == QByteArrayLiteral("filterKeys")) {
            appendFilterKey(change->addedNodeId());
            markDirty();
        } else if (change->propertyName() == QByteArrayLiteral("shaderProgram")) {
            m_shaderUuid = change->addedNodeId();
            markDirty();
        } else if (change->propertyName() == QByteArrayLiteral("renderState")) {
            addRenderState(change->addedNodeId());
            markDirty();
        } else if (change->propertyName() == QByteArrayLiteral("parameter")) {
            m_parameterPack.appendParameter(change->addedNodeId());
            markDirty();
        }
        break;
    }

    case PropertyUpdated: {
        const auto change = qSharedPointerCast<QPropertyUpdatedChange>(e);
        if (change->propertyName() == QByteArrayLiteral("shaderProgram")) {
            m_shaderUuid = change->value().value<Qt3DCore::QNodeId>();
            markDirty();
        }
        break;
    }

    case PropertyValueRemoved: {
        const auto change = qSharedPointerCast<QPropertyNodeRemovedChange>(e);
        if (change->propertyName() == QByteArrayLiteral("filterKeys")) {
            removeFilterKey(change->removedNodeId());
            markDirty();
        } else if (change->propertyName() == QByteArrayLiteral("shaderProgram")) {
            m_shaderUuid = QNodeId();
            markDirty();
        } else if (change->propertyName() == QByteArrayLiteral("renderState")) {
            removeRenderState(change->removedNodeId());
            markDirty();
        } else if (change->propertyName() == QByteArrayLiteral("parameter")) {
            m_parameterPack.removeParameter(change->removedNodeId());
            markDirty();
        }
        break;
    }

    default:
        break;
    }

    BackendNode::sceneChangeEvent(e);
}

Qt3DCore::QNodeId RenderPass::shaderProgram() const
{
    return m_shaderUuid;
}

QVector<Qt3DCore::QNodeId> RenderPass::filterKeys() const
{
    return m_filterKeyList;
}

QVector<Qt3DCore::QNodeId> RenderPass::parameters() const
{
    return m_parameterPack.parameters();
}

QVector<QNodeId> RenderPass::renderStates() const
{
    return m_renderStates;
}

void RenderPass::appendFilterKey(Qt3DCore::QNodeId filterKeyId)
{
    if (!m_filterKeyList.contains(filterKeyId))
        m_filterKeyList.append(filterKeyId);
}

void RenderPass::removeFilterKey(Qt3DCore::QNodeId filterKeyId)
{
    m_filterKeyList.removeOne(filterKeyId);
}

void RenderPass::addRenderState(QNodeId renderStateId)
{
    if (!m_renderStates.contains(renderStateId))
        m_renderStates.push_back(renderStateId);
}

void RenderPass::removeRenderState(QNodeId renderStateId)
{
    m_renderStates.removeOne(renderStateId);
}

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE
