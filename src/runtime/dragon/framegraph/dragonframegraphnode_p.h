/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT3DRENDER_DRAGON_FRAMEGRAPHNODE_H
#define QT3DRENDER_DRAGON_FRAMEGRAPHNODE_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/dragonbackendnode_p.h>

#include <QtCore/QVector>

// Windows had the smart idea of using a #define MemoryBarrier
// https://msdn.microsoft.com/en-us/library/windows/desktop/ms684208(v=vs.85).aspx
#if defined(Q_OS_WIN) && defined(MemoryBarrier)
#undef MemoryBarrier
#endif

QT_BEGIN_NAMESPACE

namespace Qt3DRender {

namespace Dragon {

class FrameGraphManager;

class FrameGraphNode : public BackendNode
{
public:
    FrameGraphNode();
    virtual ~FrameGraphNode();

    enum FrameGraphNodeType {
        InvalidNodeType = 0,
        CameraSelector,
        LayerFilter,
        RenderPassFilter,
        RenderTarget,
        TechniqueFilter,
        Viewport,
        ClearBuffers,
        SortMethod,
        SubtreeSelector,
        StateSet,
        NoDraw,
        FrustumCulling,
        Lighting,
        ComputeDispatch,
        Surface,
        RenderCapture,
        BufferCapture,
        MemoryBarrier,
        ProximityFilter,
        BlitFramebuffer
    };
    FrameGraphNodeType nodeType() const { return m_nodeType; }

    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e) override;

    FrameGraphNode(FrameGraphNodeType nodeType, Qt3DCore::QBackendNode::Mode mode = Qt3DCore::QBackendNode::ReadOnly);

private:
    FrameGraphNodeType m_nodeType;

    friend class FrameGraphVisitor;
};

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // QT3DRENDER_DRAGON_FRAMEGRAPHNODE_H
