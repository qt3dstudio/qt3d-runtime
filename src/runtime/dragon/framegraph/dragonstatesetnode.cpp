/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonstatesetnode_p.h"

#include <Qt3DRender/qrenderstateset.h>
#include <Qt3DRender/private/qrenderstateset_p.h>

#include <Qt3DCore/qpropertyupdatedchange.h>
#include <Qt3DCore/qpropertynodeaddedchange.h>
#include <Qt3DCore/qpropertynoderemovedchange.h>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

StateSetNode::StateSetNode()
    : FrameGraphNode(FrameGraphNode::StateSet)
{
}

StateSetNode::~StateSetNode()
{
}

QVector<QNodeId> StateSetNode::renderStates() const
{
    return m_renderStates;
}

void StateSetNode::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    FrameGraphNode::initializeFromPeer(change);
    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<QRenderStateSetData>>(change);
    const auto &data = typedChange->data;
    for (const auto &stateId : qAsConst(data.renderStateIds))
        addRenderState(stateId);
}

void StateSetNode::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    switch (e->type()) {
    case PropertyValueAdded: {
        const auto change = qSharedPointerCast<QPropertyNodeAddedChange>(e);
        if (change->propertyName() == QByteArrayLiteral("renderState")) {
            addRenderState(change->addedNodeId());
            markDirty();
        }
        break;
    }

    case PropertyValueRemoved: {
        const auto propertyChange = qSharedPointerCast<QPropertyNodeRemovedChange>(e);
        if (propertyChange->propertyName() == QByteArrayLiteral("renderState")) {
            removeRenderState(propertyChange->removedNodeId());
            markDirty();
        }
        break;
    }

    default:
        break;
    }
    FrameGraphNode::sceneChangeEvent(e);
}

void StateSetNode::addRenderState(QNodeId renderStateId)
{
    if (!m_renderStates.contains(renderStateId))
        m_renderStates.push_back(renderStateId);
}

void StateSetNode::removeRenderState(QNodeId renderStateId)
{
    m_renderStates.removeOne(renderStateId);
}

} // namespace Dragon

} // namespace Qt3DRender

QT_END_NAMESPACE
