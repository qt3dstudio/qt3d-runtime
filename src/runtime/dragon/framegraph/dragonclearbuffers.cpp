/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonclearbuffers_p.h"

#include <Qt3DRender/private/qclearbuffers_p.h>

#include <Qt3DCore/qpropertyupdatedchange.h>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

static QVector4D vec4dFromColor(const QColor &color)
{
    return QVector4D(color.redF(), color.greenF(), color.blueF(), color.alphaF());
}

ClearBuffers::ClearBuffers()
    : FrameGraphNode(FrameGraphNode::ClearBuffers)
    , type(QClearBuffers::None)
    , clearDepthValue(1.f)
    , clearStencilValue(0)
{
}

void ClearBuffers::initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change)
{
    FrameGraphNode::initializeFromPeer(change);
    const auto typedChange = qSharedPointerCast<Qt3DCore::QNodeCreatedChange<QClearBuffersData>>(change);
    const auto &data = typedChange->data;
    type = data.buffersType;
    clearColorAsColor = data.clearColor;
    clearColor = vec4dFromColor(clearColorAsColor);
    clearDepthValue = data.clearDepthValue;
    clearStencilValue = data.clearStencilValue;
    colorBufferId = data.bufferId;
}

void ClearBuffers::sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e)
{
    if (e->type() == PropertyUpdated) {
        QPropertyUpdatedChangePtr propertyChange = qSharedPointerCast<QPropertyUpdatedChange>(e);
        if (propertyChange->propertyName() == QByteArrayLiteral("buffers")) {
            type = static_cast<QClearBuffers::BufferType>(propertyChange->value().toInt());
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("clearColor")) {
            clearColorAsColor = propertyChange->value().value<QColor>();
            clearColor = vec4dFromColor(clearColorAsColor);
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("clearDepthValue")) {
            clearDepthValue = propertyChange->value().toFloat();
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("clearStencilValue")) {
            clearStencilValue = propertyChange->value().toInt();
            markDirty();
        } else if (propertyChange->propertyName() == QByteArrayLiteral("colorBuffer")) {
            colorBufferId = propertyChange->value().value<QNodeId>();
            markDirty();
        }
    }
    FrameGraphNode::sceneChangeEvent(e);
}

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE
