/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DRAGONNODETREE_P_H
#define DRAGONNODETREE_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

/*!
 * Qt3D passes very limited information to the backend about the child/parent hierarchy.
 * Only parent information is passed on creation and only child information is passed in updates.
 * The nodeTree tries its best to use information from the different
 * NodeFunctors (a.k.a. QBackendNodeMappers) to construct a usable tree.
 *
 * TODO: Make Qt3D pass the parent id to the backend when parent changes and/or
 * children on node creation.
 */
class NodeTree
{
public:
    struct NodeInfo
    {
        Qt3DCore::QNodeId parentId;
        QVector<Qt3DCore::QNodeId> childIds;
    };
    using Nodes = QHash<Qt3DCore::QNodeId, NodeInfo>;

    void addNode(Qt3DCore::QNodeId id)
    {
        // node might already exist if setParent or addChild was called first
        if (!m_nodes.contains(id))
            m_nodes.insert(id, NodeInfo());
    }

    void removeNode(Qt3DCore::QNodeId id)
    {
        // this is mostly to clear the data in case the node appears again
        NodeInfo node = m_nodes.take(id);
        if (!node.parentId.isNull())
            m_nodes[node.parentId].childIds.removeOne(id);
    }

    void setParent(Qt3DCore::QNodeId childId, Qt3DCore::QNodeId parentId)
    {
        // remove child from old parent
        if (m_nodes.contains(childId)) {
            Qt3DCore::QNodeId oldParentId = m_nodes[childId].parentId;
            if (m_nodes.contains(oldParentId)) {
                m_nodes[oldParentId].childIds.removeOne(childId);
            }
        }
        // using operator[] here on purpose - we want to set it even if addNode has not been called
        m_nodes[childId].parentId = parentId;
        if (!parentId.isNull() && !m_nodes[parentId].childIds.contains(childId))
            m_nodes[parentId].childIds.push_back(childId);
    }

    const QHash<Qt3DCore::QNodeId, NodeInfo> &nodes()
    {
        return m_nodes;
    }

    // Task interface
    QHash<Qt3DCore::QNodeId, NodeInfo> result()
    {
        return m_nodes;
    }

    bool isFinished()
    {
        return true;
    }

private:
    QHash<Qt3DCore::QNodeId, NodeInfo> m_nodes;
};

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // DRAGONNODETREE_P_H
