/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonrenderviewjobs_p.h"

// Frame graph
#include <private/dragonblitframebuffer_p.h>
#include <private/dragonbuffercapture_p.h>
#include <private/dragoncameraselectornode_p.h>
#include <private/dragonclearbuffers_p.h>
#include <private/dragondispatchcompute_p.h>
#include <private/dragonfrustumculling_p.h>
#include <private/dragonlayerfilternode_p.h>
#include <private/dragonmemorybarrier_p.h>
#include <private/dragonnodraw_p.h>
#include <private/dragonproximityfilter_p.h>
#include <private/dragonrendercapture_p.h>
#include <private/dragonrenderpassfilternode_p.h>
#include <private/dragonrendersurfaceselector_p.h>
#include <private/dragonrendertargetselectornode_p.h>
#include <private/dragonshaderdata_p.h>
#include <private/dragonsortpolicy_p.h>
#include <private/dragonstatesetnode_p.h>
#include <private/dragontechniquefilternode_p.h>
#include <private/dragonviewportnode_p.h>

// Nodes
#include <private/dragoneffect_p.h>
#include <private/dragonfilterkey_p.h>
#include <private/dragonmaterial_p.h>
#include <private/dragonparameter_p.h>
#include <private/dragonrenderpass_p.h>
#include <private/dragonrenderstatenode_p.h>
#include <private/dragonrenderstates_p.h>
#include <private/dragonrenderstateset_p.h>
#include <private/dragonrendertarget_p.h>
#include <private/dragonrendertargetoutput_p.h>
#include <private/dragontechnique_p.h>
#include <private/dragontexture_p.h>

// Internal
#include <private/dragonrendercommand_p.h>
#include <private/dragonsphere_p.h>
#include <private/dragontransformjobs_p.h>
#include <private/dragonrenderview_p.h>
#include <private/dragonboundingvolumejobs_p.h>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

AttachmentPack createAttachmentPack(const Immutable<RenderTarget> &renderTarget,
                                    const ValueContainer<RenderTargetOutput> &renderTargetOutputs,
                                    const QVector<QRenderTargetOutput::AttachmentPoint> &drawBuffers = {})
{
    const auto outputIds = renderTarget->renderOutputs();
    QVector<Immutable<RenderTargetOutput>> attachments;
    for (Qt3DCore::QNodeId outputId : outputIds) {
        Q_ASSERT(renderTargetOutputs.contains(outputId));
        const auto renderTargetOutput = renderTargetOutputs[outputId];
        attachments.push_back(renderTargetOutput);
    }
    AttachmentPack pack;
    pack.outputs = attachments;

    // If nothing is specified, use all the attachments as draw buffers
    if (drawBuffers.isEmpty()) {
        pack.drawBuffers.reserve(attachments.size());
        for (const auto &attachment : qAsConst(attachments)) {
            // only consider color outputs
            if (attachment->point > QRenderTargetOutput::Color15)
                continue;
            pack.drawBuffers.push_back(int(attachment->point));
        }
    } else {
        pack.drawBuffers.reserve(drawBuffers.size());
        for (const auto &output : drawBuffers) {
            // only consider color outputs
            if (output > QRenderTargetOutput::Color15)
                continue;
            pack.drawBuffers.push_back(int(output));
        }
    }
    return pack;
}

// TODO would be nice to have the nodes in the TreeInfo
RenderViews buildRenderViews(RenderViews renderViews,
                             const TreeInfo &frameGraph,
                             const ValueContainer<FrameGraphNode> &frameGraphNodes,
                             const ValueContainer<Entity> &entities,
                             const ValueContainer<CameraLens> &cameraLenses,
                             const ValueContainer<RenderTarget> &renderTargets,
                             const ValueContainer<RenderTargetOutput> &renderTargetOutputs,
                             const ValueContainer<RenderStateNode> &renderStates)
//                             const ValueContainer<Matrix4x4> &worldTransforms)
{
    renderViews.reset();

    bool needsRebuild = frameGraph.nodes.anythingDirty() || frameGraphNodes.anythingDirty()
            || entities.anythingDirty() || cameraLenses.anythingDirty()
            || renderTargets.anythingDirty() || renderTargetOutputs.anythingDirty()
            || renderStates.anythingDirty();

    if (!needsRebuild)
        return renderViews;

    for (const auto &leafNodeId : frameGraph.leafNodes) {
        bool hasRenderStateSet = false;
        RenderView renderView;
        renderView.leafNodeId = leafNodeId;
        const auto &ancestors = frameGraph.nodes[leafNodeId]->ancestors;
        // TODO consider adding an ancestor list that includes ourselves
        const auto ancestorsAndMe = QVector<QNodeId>{ { leafNodeId } } + ancestors;
        for (const auto &ancestorId : ancestorsAndMe) {
            const auto &node = frameGraphNodes[ancestorId];
            if (!node->isEnabled())
                continue;

            switch (node->nodeType()) {
            case FrameGraphNode::InvalidNodeType:
                // A base FrameGraphNode, can be used for grouping purposes
                break;
            case FrameGraphNode::CameraSelector: {
                // Can be set only once and we take camera nearest to the leaf node
                if (renderView.hasCamera)
                    continue;
                Q_ASSERT(node.can_convert<CameraSelector>());
                const auto cameraSelector = node.as<CameraSelector>();
                if (cameraSelector->cameraUuid().isNull())
                    continue;

                renderView.hasCamera = true;
                Q_ASSERT(entities.contains(cameraSelector->cameraUuid()));
                const auto &cameraNode = entities[cameraSelector->cameraUuid()];
                const auto &cameraLens = cameraLenses[cameraNode->cameraLensComponent()];

                renderView.cameraNode = cameraNode;
                renderView.cameraLens = cameraLens;

                // TODO consider storing only the lens id to avoid rebuilding on projectionMatrix changes

//                renderView.projectionMatrix = cameraLens->projectionMatrix();

                // TODO Performing these operations in a different job allows us to not rebuild renderviews if transforms are dirty
//                const Matrix4x4 cameraWorld = Matrix4x4(*(worldTransforms[cameraNode->peerId()]));
//                const auto &viewMatrix = cameraLens->viewMatrix(cameraWorld);
//                renderView.viewMatrix = viewMatrix;
//                renderView.viewProjectionMatrix = cameraLens->projectionMatrix() * viewMatrix;

//                //To get the eyePosition of the camera, we need to use the inverse of the
//                // camera's worldTransform matrix.
//                const Matrix4x4 inverseWorldTransform = viewMatrix.inverted();
//                const Vector3D eyePosition(inverseWorldTransform.column(3));
//                renderView.eyePosition = eyePosition;

//                // Get the viewing direction of the camera. Use the normal matrix to
//                // ensure non-uniform scale works too.
//                const QMatrix3x3 normalMat = convertToQMatrix4x4(viewMatrix).normalMatrix();
//                // dir = normalize(QVector3D(0, 0, -1) * normalMat)
//                renderView.eyeViewDirection = Vector3D(-normalMat(2, 0), -normalMat(2, 1), -normalMat(2, 2)).normalized();
                break;
            }
            case FrameGraphNode::LayerFilter: {
                // Can be set multiple times in the tree
                Q_ASSERT(node.can_convert<LayerFilterNode>());
                const auto &layerFilter = node.as<LayerFilterNode>();
                renderView.layerFilters.push_back(layerFilter);
                break;
            }

                //            case FrameGraphNode::ProximityFilter: // Can be set multiple times in
                //            the tree
                //                rv->appendProximityFilterId(node->peerId());
                //                break;

            case FrameGraphNode::RenderPassFilter: {
                // Can be set once
                Q_ASSERT(node.can_convert<RenderPassFilter>());
                const auto &renderPassFilter = node.as<RenderPassFilter>();

                if (!renderView.hasRenderPassFilter) {
                    renderView.renderPassFilter = renderPassFilter;
                    renderView.hasRenderPassFilter = true;
                }
                break;
            }
            case FrameGraphNode::RenderTarget: {
                // Can be set once and we take render target nearest to the leaf
                if (!renderView.renderTargetId.isNull())
                    continue;

                Q_ASSERT(node.can_convert<Dragon::RenderTargetSelector>());
                const auto targetSelector = node.as<Dragon::RenderTargetSelector>();
                QNodeId renderTargetUid = targetSelector->renderTargetUuid();
                // If it is not referring to a real render target, just continue
                if (renderTargetUid.isNull())
                    continue;

                Q_ASSERT(renderTargets.contains(renderTargetUid));
                auto renderTarget = renderTargets[renderTargetUid];

                // Add renderTarget Handle and build renderCommand AttachmentPack
                renderView.renderTargetId = renderTargetUid;

                // Find attachments
                AttachmentPack pack = createAttachmentPack(renderTarget, renderTargetOutputs, targetSelector->outputs());
                renderView.attachmentPack = pack;
                break;
            }

            case FrameGraphNode::ClearBuffers: {
                Q_ASSERT(node.can_convert<Dragon::ClearBuffers>());
                const auto cb = node.as<Dragon::ClearBuffers>();

                ClearBackBufferInfo info;
                QClearBuffers::BufferTypeFlags type = cb->type;

                if (type & QClearBuffers::StencilBuffer) {
                    info.stencil = cb->clearStencilValue;
                    info.buffers |= QClearBuffers::StencilBuffer;
                }
                if (type & QClearBuffers::DepthBuffer) {
                    info.depth = cb->clearDepthValue;
                    info.buffers |= QClearBuffers::DepthBuffer;
                }
                // keep track of global ClearColor (if set) and collect all DrawBuffer-specific
                // ClearColors
                if (type & QClearBuffers::ColorBuffer) {
                    if (cb->colorBufferId.isNull()) {
                        info.color = cb->clearColor;
                        info.buffers |= QClearBuffers::ColorBuffer;
                    } else {
                        ClearColorInfo clearColorInfo;
                        clearColorInfo.clearColor = cb->clearColor;
                        qWarning() << "WARNING: Reached incomplete code!";
                        // TODO add back attachmentPoint
                        //                        clearColorInfo.attchmentPoint =
                        //                        targetOutput->point();
                        // Note: a job is later performed to find the drawIndex from the buffer
                        // attachment point using the AttachmentPack
                        renderView.specificClearColorBuffers.insert(cb->colorBufferId, clearColorInfo);
                    }
                }

                renderView.clearBackBufferInfo = info;
                break;
            }

            case FrameGraphNode::TechniqueFilter: {
                // Can be set once
                Q_ASSERT(node.can_convert<TechniqueFilter>());
                const auto &techniqueFilter = node.as<TechniqueFilter>();
                // TODO Amalgamate all technique filters from leaf to root

                if (!renderView.hasTechniqueFilter) {
                    renderView.techniqueFilter = techniqueFilter;
                    renderView.hasTechniqueFilter = true;
                }
                break;
            }
            case FrameGraphNode::Viewport: {
                // If the Viewport has already been set in a lower node
                // Make it so that the new viewport is actually
                // a subregion relative to that of the parent viewport
                Q_ASSERT(node.can_convert<Dragon::ViewportNode>());
                const auto &vpNode = node.as<Dragon::ViewportNode>();
                renderView.viewport = Dragon::ViewportNode::computeViewport(renderView.viewport,
                                                                            *vpNode);
                renderView.gamma = vpNode->gamma();
                break;
            }

            case FrameGraphNode::SortMethod: {
                Q_ASSERT(node.can_convert<Dragon::SortPolicy>());
                const auto &sortPolicy = node.as<Dragon::SortPolicy>();
                renderView.sortingTypes.append(sortPolicy->sortTypes());
                break;
            }

                //            case FrameGraphNode::SubtreeSelector:
                //                // Has no meaning here. SubtreeSelector was used
                //                // in a prior step to build the list of RenderViewJobs
                //                break;

            case FrameGraphNode::StateSet: {
                Q_ASSERT(node.can_convert<StateSetNode>());
                hasRenderStateSet = true;
                const auto &stateSet = node.as<StateSetNode>();
                auto renderViewStateSet = *renderView.renderStateSet;
                for (const Qt3DCore::QNodeId &stateId : stateSet->renderStates()) {
                    Q_ASSERT(renderStates.contains(stateId));
                    const auto &renderStateNode = renderStates[stateId];
                    if (renderStateNode->isEnabled())
                        renderViewStateSet.addState(renderStateNode->impl());
                }
                renderView.renderStateSet = renderViewStateSet;
                break;
            }

            case FrameGraphNode::NoDraw: {
                renderView.noDraw = true;
                break;
            }

                //            case FrameGraphNode::FrustumCulling: {
                //                rv->setFrustumCulling(true);
                //                break;
                //            }

                //            case FrameGraphNode::ComputeDispatch: {
                //                const Render::DispatchCompute *dispatchCompute =
                //                    static_cast<const Render::DispatchCompute *>(node);
                //                rv->setCompute(true);
                //                rv->setComputeWorkgroups(dispatchCompute->x(),
                //                dispatchCompute->y(),
                //                                         dispatchCompute->z());
                //                break;
                //            }

                //            case FrameGraphNode::Lighting: {
                //                // TODO
                //                break;
                //            }

            case FrameGraphNode::Surface: {
                if (renderView.surface != nullptr)
                    continue;
                Q_ASSERT(node.can_convert<Dragon::RenderSurfaceSelector>());
                const auto surfaceSelector = node.as<Dragon::RenderSurfaceSelector>();
                renderView.surface = surfaceSelector->surface();
                renderView.surfaceSize = surfaceSelector->renderTargetSize();
                renderView.devicePixelRatio = surfaceSelector->devicePixelRatio();
                break;
            }
                //            case FrameGraphNode::RenderCapture: {
                //                auto *renderCapture = const_cast<Render::RenderCapture *>(
                //                    static_cast<const Render::RenderCapture *>(node));
                //                if (rv->renderCaptureNodeId().isNull() &&
                //                renderCapture->wasCaptureRequested()) {
                //                    rv->setRenderCaptureNodeId(renderCapture->peerId());
                //                    rv->setRenderCaptureRequest(renderCapture->takeCaptureRequest());
                //                }
                //                break;
                //            }

                //            case FrameGraphNode::MemoryBarrier: {
                //                const Render::MemoryBarrier *barrier =
                //                    static_cast<const Render::MemoryBarrier *>(node);
                //                rv->setMemoryBarrier(barrier->waitOperations() |
                //                rv->memoryBarrier()); break;
                //            }

                //            case FrameGraphNode::BufferCapture: {
                //                auto *bufferCapture = const_cast<Render::BufferCapture *>(
                //                    static_cast<const Render::BufferCapture *>(node));
                //                if (bufferCapture != nullptr)
                //                    rv->setIsDownloadBuffersEnable(bufferCapture->isEnabled());
                //                break;
                //            }

            case FrameGraphNode::BlitFramebuffer: {
                Q_ASSERT(node.can_convert<BlitFramebuffer>());
                const auto &blitFramebufferNode = node.as<BlitFramebuffer>();
                BlitFramebufferInfo bfbInfo;
                if (!blitFramebufferNode->sourceRenderTargetId().isNull()) {
                    Q_ASSERT(renderTargets.contains(blitFramebufferNode->sourceRenderTargetId()));
                    const auto &sourceRenderTarget = renderTargets[blitFramebufferNode->sourceRenderTargetId()];
                    bfbInfo.sourceRenderTarget = sourceRenderTarget;
                    bfbInfo.sourceAttachments = createAttachmentPack(sourceRenderTarget, renderTargetOutputs);
                }
                if (!blitFramebufferNode->destinationRenderTargetId().isNull()) {
                    Q_ASSERT(renderTargets.contains(blitFramebufferNode->destinationRenderTargetId()));
                    const auto &destinationRenderTarget = renderTargets[blitFramebufferNode->destinationRenderTargetId()];
                    bfbInfo.destinationRenderTarget = destinationRenderTarget;
                    bfbInfo.destinationAttachments = createAttachmentPack(destinationRenderTarget, renderTargetOutputs);
                }
                bfbInfo.node = blitFramebufferNode;
                renderView.blitFrameBufferInfo = bfbInfo;
                break;
            }
            default: {
                // TODO add back this warning once we cache results again
                //                qWarning() << "WARNING: Frame graph node not supported in Dragon renderer:"
                //                           << node->nodeType();
                break;
            }
            }
        }

        // TODO consider adding back this warning this, but it needs to make sense when using deferred rendering
        //        if (!renderView.hasCamera && !renderView.noDraw)
        //            qWarning()
        //                << "[Qt3D Renderer] No Camera Lens found. Add a CameraSelector to your Frame Graph or make sure that no entities will be rendered.";

        // Verify that the render view is usable
        if (renderView.surface == nullptr) {
            qWarning() << "WARNING: Found leaf node in frame graph without surface. Will not render.";
            continue;
        }

        // If no render states were set, assume the following defaults
        // TODO add documentation
        if (!hasRenderStateSet)
            renderView.renderStateSet = RenderStateSet::defaultRenderStateSet();

        // TODO do not just assume camera has been set
        renderViews[leafNodeId] = renderView;
    }

    // Remove render views that are no longer in the list of leaf nodes
    const auto keys = renderViews.keys();
    for (const auto &id : keys) {
        if (!frameGraph.leafNodes.contains(id)) {
            renderViews.remove(id);
        }
    }

    return renderViews;
} // namespace Dragon

enum StandardUniform {
    ModelMatrix,
    ViewMatrix,
    ProjectionMatrix,
    ModelViewMatrix,
    ViewProjectionMatrix,
    ModelViewProjectionMatrix,
    InverseModelMatrix,
    InverseViewMatrix,
    InverseProjectionMatrix,
    InverseModelViewMatrix,
    InverseViewProjectionMatrix,
    InverseModelViewProjectionMatrix,
    ModelNormalMatrix,
    ModelViewNormalMatrix,
    ViewportMatrix,
    InverseViewportMatrix,
    AspectRatio,
    Time,
    Exposure,
    Gamma,
    EyePosition,
    SkinningPalette
};

// We accept the entity if it contains any of the layers that are in the layer
// filter
bool containsAnyMatchingLayers(const Immutable<Entity> &entity,
                               const Qt3DCore::QNodeIdVector &layerIds)
{
    const Qt3DCore::QNodeIdVector entityLayers = entity->m_layerComponents;

    for (const Qt3DCore::QNodeId id : entityLayers) {
        if (layerIds.contains(id))
            return true;
    }
    return false;
}

// We accept the entity if it contains all the layers that are in the layer
// filter
bool containsAllMatchingLayers(const Immutable<Entity> &entity,
                               const Qt3DCore::QNodeIdVector &layerIds)
{
    const Qt3DCore::QNodeIdVector entityLayers = entity->m_layerComponents;

    for (const Qt3DCore::QNodeId id : layerIds) {
        if (!entityLayers.contains(id))
            return false;
    }
    return true;
}

// TODO move to a separate file to reduce the size of this file
namespace {

template<int SortType>
struct AdjacentSubRangeFinder {
    static bool adjacentSubRange(const Immutable<RenderCommand> &, const Immutable<RenderCommand> &)
    {
        Q_UNREACHABLE();
        return false;
    }
};

template<>
struct AdjacentSubRangeFinder<QSortPolicy::StateChangeCost> {
    static bool adjacentSubRange(const Immutable<RenderCommand> &a, const Immutable<RenderCommand> &b)
    {
        return a->m_changeCost == b->m_changeCost;
    }
};

template<>
struct AdjacentSubRangeFinder<QSortPolicy::BackToFront> {
    static bool adjacentSubRange(const Immutable<RenderCommand> &a, const Immutable<RenderCommand> &b)
    {
        return a->m_depth == b->m_depth;
    }
};

template<>
struct AdjacentSubRangeFinder<QSortPolicy::Material> {
    static bool adjacentSubRange(const Immutable<RenderCommand> &a, const Immutable<RenderCommand> &b)
    {
        return a->m_shaderDna == b->m_shaderDna;
    }
};

template<>
struct AdjacentSubRangeFinder<QSortPolicy::FrontToBack> {
    static bool adjacentSubRange(const Immutable<RenderCommand> &a, const Immutable<RenderCommand> &b)
    {
        return a->m_depth == b->m_depth;
    }
};

template<typename Predicate>
int advanceUntilNonAdjacent(const QVector<Immutable<RenderCommand>> &commands,
                            const int beg, const int end, Predicate pred)
{
    int i = beg + 1;
    while (i < end) {
        if (!pred(*(commands.begin() + beg), *(commands.begin() + i)))
            break;
        ++i;
    }
    return i;
}

using CommandIt = QVector<Immutable<RenderCommand>>::iterator;

template<int SortType>
struct SubRangeSorter {
    static void sortSubRange(CommandIt begin, const CommandIt end)
    {
        Q_UNUSED(begin);
        Q_UNUSED(end);
        Q_UNREACHABLE();
    }
};

template<>
struct SubRangeSorter<QSortPolicy::StateChangeCost> {
    static void sortSubRange(CommandIt begin, const CommandIt end)
    {
        std::stable_sort(begin, end, [](const Immutable<RenderCommand> &a, const Immutable<RenderCommand> &b) {
            return a->m_changeCost > b->m_changeCost;
        });
    }
};

template<>
struct SubRangeSorter<QSortPolicy::BackToFront> {
    static void sortSubRange(CommandIt begin, const CommandIt end)
    {
        std::stable_sort(begin, end, [](const Immutable<RenderCommand> &a, const Immutable<RenderCommand> &b) {
            return a->m_depth > b->m_depth;
        });
    }
};

template<>
struct SubRangeSorter<QSortPolicy::Material> {
    static void sortSubRange(CommandIt begin, const CommandIt end)
    {
        // First we sort by shaderDNA
        std::stable_sort(begin, end, [](const Immutable<RenderCommand> &a, const Immutable<RenderCommand> &b) {
            return a->m_shaderDna > b->m_shaderDna;
        });
    }
};

template<>
struct SubRangeSorter<QSortPolicy::FrontToBack> {
    static void sortSubRange(CommandIt begin, const CommandIt end)
    {
        std::stable_sort(begin, end, [](const Immutable<RenderCommand> &a, const Immutable<RenderCommand> &b) {
            return a->m_depth < b->m_depth;
        });
    }
};

int findSubRange(const QVector<Immutable<RenderCommand>> &commands,
                 const int begin, const int end,
                 const QSortPolicy::SortType sortType)
{
    switch (sortType) {
    case QSortPolicy::StateChangeCost:
        return advanceUntilNonAdjacent(commands, begin, end, AdjacentSubRangeFinder<QSortPolicy::StateChangeCost>::adjacentSubRange);
    case QSortPolicy::BackToFront:
        return advanceUntilNonAdjacent(commands, begin, end, AdjacentSubRangeFinder<QSortPolicy::BackToFront>::adjacentSubRange);
    case QSortPolicy::Material:
        return advanceUntilNonAdjacent(commands, begin, end, AdjacentSubRangeFinder<QSortPolicy::Material>::adjacentSubRange);
    case QSortPolicy::FrontToBack:
        return advanceUntilNonAdjacent(commands, begin, end, AdjacentSubRangeFinder<QSortPolicy::FrontToBack>::adjacentSubRange);
    default:
        Q_UNREACHABLE();
        return end;
    }
}

void sortByMaterial(QVector<Immutable<RenderCommand>> &commands, int begin, const int end)
{
    Q_UNUSED(commands);
    Q_UNUSED(begin);
    Q_UNUSED(end);

    //    // We try to arrange elements so that their rendering cost is minimized for a given shader
    //    int rangeEnd = advanceUntilNonAdjacent(commands, begin, end, AdjacentSubRangeFinder<QSortPolicy::Material>::adjacentSubRange);
    //    while (begin != end) {
    //        if (begin + 1 < rangeEnd) {
    //            std::stable_sort(commands.begin() + begin + 1, commands.begin() + rangeEnd, [] (RenderCommand *a, RenderCommand *b){
    //                return a->m_material.handle() < b->m_material.handle();
    //            });
    //        }
    //        begin = rangeEnd;
    //        rangeEnd = advanceUntilNonAdjacent(commands, begin, end, AdjacentSubRangeFinder<QSortPolicy::Material>::adjacentSubRange);
    //    }
    qWarning() << "WARNING: SortByMaterial not supported";
}

// TODO make into an input->output function instead of modifying input argument
void sortCommandRange(QVector<Immutable<RenderCommand>> &commands, int begin, const int end, const int level,
                      const QVector<Qt3DRender::QSortPolicy::SortType> &sortingTypes)
{
    if (level >= sortingTypes.size())
        return;

    switch (sortingTypes.at(level)) {
    case QSortPolicy::StateChangeCost:
        SubRangeSorter<QSortPolicy::StateChangeCost>::sortSubRange(commands.begin() + begin, commands.begin() + end);
        break;
    case QSortPolicy::BackToFront:
        SubRangeSorter<QSortPolicy::BackToFront>::sortSubRange(commands.begin() + begin, commands.begin() + end);
        break;
    case QSortPolicy::Material:
        // Groups all same shader DNA together
        SubRangeSorter<QSortPolicy::Material>::sortSubRange(commands.begin() + begin, commands.begin() + end);
        // Group all same material together (same parameters most likely)
        sortByMaterial(commands, begin, end);
        break;
    case QSortPolicy::FrontToBack:
        SubRangeSorter<QSortPolicy::FrontToBack>::sortSubRange(commands.begin() + begin, commands.begin() + end);
        break;
    default:
        Q_UNREACHABLE();
    }

    // For all sub ranges of adjacent item for sortType[i]
    // Perform filtering with sortType[i + 1]
    int rangeEnd = findSubRange(commands, begin, end, sortingTypes.at(level));
    while (begin != end) {
        sortCommandRange(commands, begin, rangeEnd, level + 1, sortingTypes);
        begin = rangeEnd;
        rangeEnd = findSubRange(commands, begin, end, sortingTypes.at(level));
    }
}

} // anonymous

RenderCommands buildDrawRenderCommands(RenderCommands renderCommands,
                                       const ValueContainer<RenderView> &renderViews,
                                       const ValueContainer<Entity> &entities,
                                       const ValueContainer<Material> &materials,
                                       const ValueContainer<Geometry> &geometries,
                                       const ValueContainer<GeometryRenderer> &geometryRenderers,
                                       const ValueContainer<Shader> &shaders,
                                       const ValueContainer<RenderStateNode> &renderStates,
                                       const GatheredParameters &gatheredParameters)
{
    renderCommands.reset();

    bool needsRebuild = renderViews.anythingDirty() || entities.anythingDirty()
            || materials.anythingDirty() || geometries.anythingDirty()
            || geometryRenderers.anythingDirty() || shaders.anythingDirty()
            || renderStates.anythingDirty()
            || gatheredParameters.anythingDirty();

    if (!needsRebuild)
        return renderCommands;

    auto commandsForView = [=](const QNodeId &id, const Immutable<RenderView> &renderView) {
        RenderViewCommands result;
        result.renderView = renderView;
        if (renderView->noDraw) {
            return result;
        }
        // TODO don't use a reference here, just build the vector and set it on the result after
        auto &commands = result.commands;
        commands.reserve(int(entities.size()));

        // TODO this should be done in a job long before we get here
        auto filteredEntities = ValueContainer<Entity>();
        for (const auto &entityId : entities.keys()) {
            const auto &entity = entities[entityId];
            if (entity->m_geometryRendererComponent.isNull())
                continue;

            // TODO consider if this is the correct behavior (to hide when disabled)
            if (!entity->isEnabled())
                continue;

            bool accepted = true;
            for (const auto &layerFilter : renderView->layerFilters) {
                const auto &filterLayers = layerFilter->layerIds;
                if (!containsAnyMatchingLayers(entity, filterLayers))
                    accepted = false;
            }

            if (accepted)
                filteredEntities[entityId] = entity;
        }

        for (const auto &entity : filteredEntities) {
            if (entity->m_geometryRendererComponent.isNull())
                continue;
            Q_ASSERT(geometryRenderers.contains(entity->m_geometryRendererComponent));
            Immutable<GeometryRenderer> geometryRenderer = geometryRenderers[entity->m_geometryRendererComponent];

            // There is a geometry renderer with geometry
            if (!geometryRenderer->isEnabled() || geometryRenderer->geometryId().isNull()) {
                continue;
            }

            const auto &material = materials[entity->m_materialComponent];
            Q_ASSERT(gatheredParameters.contains(id)); // id = leaf node id, aka render view id
            const auto &renderViewParameterIds = gatheredParameters[id];
            const auto &materialParameterIds = renderViewParameterIds->materialParameterIds[material->peerId()];
            const auto &geometry = geometries[geometryRenderer->geometryId()];

            // TODO consider making material parameter gatherer output combinator combination of
            // render view * material * render pass with own ids, where each would correspond to
            // a render command
            // 1 RenderCommand per RenderPass on an Entity with a Mesh
            for (const auto &passData : materialParameterIds.renderPassParameterIds) {
                // Add the RenderPass Parameters
                RenderCommand command;
                // TODO add depth information
                //                command.m_depth = Vector3D::dotProduct(entity->worldBoundingVolume()->center() - m_data.m_eyePos, m_data.m_eyeViewDir);
//                const auto &entityCenter = boundingVolumes[entity->peerId()]->world.center();
//                const auto &eyePosition = renderView->eyePosition;
//                const auto &eyeViewDirection = renderView->eyeViewDirection;
//                command.m_depth = Vector3D::dotProduct(entityCenter - eyePosition, eyeViewDirection);
                command.m_entity = entity;
                //                command.m_worldTransform = worldTransforms[entity->peerId()];
                command.m_geometry = geometry;
                command.m_geometryRenderer = geometryRenderer;
                command.m_material = material;

                command.m_parameterIds = passData.parameterIds;

                const auto &renderPass = passData.renderPass;

                // TODO move assert inside lookup function
                Q_ASSERT(shaders.contains(renderPass->shaderProgram()));
                const auto &shader = shaders[renderPass->shaderProgram()];
                command.m_shader = shader;

                // Set fragData Name and index
                // Later on we might want to relink the shader if attachments have changed
                // But for now we set them once and for all
                QHash<QString, int> fragOutputs;
                if (!renderView->renderTargetId.isNull()) {
                    const auto outputs = renderView->attachmentPack.outputs;
                    for (const auto &output : outputs) {
                        if (output->point <= QRenderTargetOutput::Color15)
                            fragOutputs.insert(output->name, output->point);
                    }
                }
                command.m_fragOutputs = fragOutputs;

                // Project the camera-to-object-center vector onto the camera
                // view vector. This gives a depth value suitable as the key
                // for BackToFront sorting.

                // TODO we need bounding volume for entities
                //                command.m_depth = Vector3D::dotProduct(
                //                            entity->worldBoundingVolume()->center() - m_data.m_eyePos,
                //                            m_data.m_eyeViewDir);

                // For RenderPass based states we use the globally set RenderState
                // if no renderstates are defined as part of the pass. That means:
                // RenderPass { renderStates: [] } will use the states defined by
                // StateSet in the FrameGraph

                RenderStateSet renderStateSet = renderView->renderStateSet.get();

                if (renderPass->hasRenderStates()) {
                    for (const Qt3DCore::QNodeId &stateId : renderPass->renderStates()) {
                        Q_ASSERT(renderStates.contains(stateId));
                        const auto &renderStateNode = renderStates[stateId];
                        if (renderStateNode->isEnabled())
                            renderStateSet.addState(renderStateNode->impl());
                    }

                    // TODO consider adding back change cost
                    // command.changeCost = defaultRenderState->changeCost(renderStateSet);
                }

                command.m_renderStateSet = renderStateSet;

                commands.append(command);
            }
        }

        // Sort the commands
//        sortCommandRange(commands, 0, commands.size(), 0, renderView->sortingTypes);

        return result;
    };

    // TODO add back case where not all needs to be rebuilt
    if (needsRebuild)
        renderCommands = rebuildAll(std::move(renderCommands), renderViews, commandsForView);
    else
        renderCommands = synchronizeKeys(std::move(renderCommands), renderViews, commandsForView);

    return renderCommands;
}

RenderCommands sortRenderCommands(RenderCommands renderCommands,
                                  RenderCommands renderCommandsIn,
                                  const WorldBoundingVolumes &worldBoundingVolumes,
                                  const ValueContainer<CameraMatrices> &renderViewCameraMatrices)
{
    // TODO renderViewCameraMatrices not used?
    if (!renderCommandsIn.anythingDirty() && !worldBoundingVolumes.anythingDirty() && !renderViewCameraMatrices.anythingDirty())
        return renderCommands;

    auto sortRenderCommandsForView =  [worldBoundingVolumes, renderViewCameraMatrices](QNodeId leafNodeId, const Immutable<RenderViewCommands> &renderViewCommands) {
        auto cameraMatrices = renderViewCameraMatrices[leafNodeId];
        auto commands = renderViewCommands->commands;
        auto commands2 = commands;
        const auto &renderView = renderViewCommands->renderView;

        // TODO ugh, ugly because we need to change the command's depth value for sorting
        commands2.clear();
        commands2.reserve(commands.size());
        for (const auto &command : commands) {
            auto command2 = *command;
            const auto &entity = command->m_entity;
            const auto &entityCenter = worldBoundingVolumes[entity->peerId()]->world.center();
            const auto &eyePosition = cameraMatrices->eyePosition;
            const auto &eyeViewDirection = cameraMatrices->eyeViewDirection;
            command2.m_depth = Vector3D::dotProduct(entityCenter - eyePosition, eyeViewDirection);
            commands2.push_back(command2);
        }

        sortCommandRange(commands2, 0, commands2.size(), 0, renderView->sortingTypes);

        RenderViewCommands result;
        result.renderView = renderView;
        result.commands = commands2;

        return result;
    };

    renderCommands = rebuildAll(std::move(renderCommands), renderCommandsIn, sortRenderCommandsForView);

    return renderCommands;
}

Immutable<Technique> findTechniqueForEffect(const ValueContainer<Technique> &techniques,
                                        const ValueContainer<FilterKey> &filterKeys,
                                        const Immutable<TechniqueFilter> techniqueFilter,
                                        const Immutable<Effect> &effect,
                                        const GraphicsApiFilterData &contextInfo)
{
    auto isCompatibleWithFilters = [filterKeys](const Immutable<Technique> &technique,
                                                const QNodeIdVector &filterKeyIds) {
        // TODO strictly not necessary, we should fall through below anyways
        // If there are no filters, then we are compatible
        if (filterKeyIds.isEmpty())
            return true;

        // There is a technique filter so we need to check for a technique with suitable criteria.
        // Check for early bail out if the technique doesn't have sufficient number of criteria and
        // can therefore never satisfy the filter
        if (technique->filterKeys().size() < filterKeyIds.size())
            return false;

        // Iterate through the filter criteria and for each one search for a criteria on the
        // technique that satisfies it
        for (const auto &filterKeyId : filterKeyIds) {
            const auto &filterKey = filterKeys[filterKeyId];
            bool foundMatch = false;
            const auto &techniqueFilterKeys = technique->filterKeys();
            for (const auto &techniqueFilterKeyId : techniqueFilterKeys) {
                if (filterKeyId == techniqueFilterKeyId) {
                    foundMatch = true;
                    break;
                }
                const auto &techniqueFilterKey = filterKeys[techniqueFilterKeyId];
                if (*techniqueFilterKey == *filterKey) {
                    foundMatch = true;
                    break;
                }
            }

            // No match for TechniqueFilter criterion in any of the technique's criteria.
            // So no way this can match. Don't bother checking the rest of the criteria.
            if (!foundMatch)
                return false;
        }
        return true;
    };

    QVector<Immutable<Technique>> matchingTechniques;

    // Iterate through the techniques in the effect
    const auto techniqueIds = effect->techniques();
    for (const QNodeId &techniqueId : techniqueIds) {
        Q_ASSERT(techniques.contains(techniqueId));
        const auto &technique = techniques[techniqueId];

        // Check if the technique is compatible with the rendering API
        // If no techniqueFilter is present, we return the technique as it satisfies OpenGL version

        bool isCompatibleWithRenderer = (contextInfo == technique->graphicsApiFilter());

        if (isCompatibleWithRenderer
            && isCompatibleWithFilters(technique, techniqueFilter->filters()))
            matchingTechniques.append(technique);
    }

    if (matchingTechniques.size() == 0) {
        return {};
    }

    if (matchingTechniques.size() == 1)
        return matchingTechniques.first();

    // Several compatible techniques, return technique with highest major and minor version
    auto highest = matchingTechniques.first();
    GraphicsApiFilterData filter = highest->graphicsApiFilter();
    for (auto &technique : matchingTechniques) {
        if (filter < technique->graphicsApiFilter()) {
            filter = technique->graphicsApiFilter();
            highest = technique;
        }
    }
    return highest;
}

using RenderPassList = QVector<Immutable<RenderPass>>;
RenderPassList findRenderPassesForTechnique(const ValueContainer<RenderPass> &renderPasses,
                                            const ValueContainer<FilterKey> &filterKeys,
                                            const Immutable<RenderPassFilter> &passFilter,
                                            const Immutable<Technique> &technique)
{
    RenderPassList passes;
    const auto passIds = technique->renderPasses();

    for (const QNodeId &passId : passIds) {
        Q_ASSERT(renderPasses.contains(passId));
        const auto &renderPass = renderPasses[passId];

        if (!renderPass->isEnabled()) {
            continue;
        }

        if (passFilter->filters().size() == 0) {
            passes << renderPass;
            continue;
        }

        // A pass filter is present so we need to check for matching criteria
        if (renderPass->filterKeys().size() < passFilter->filters().size()) {
            continue;
        }

        // Iterate through the filter criteria and look for render passes with criteria that satisfy
        // them
        bool foundMatch = false;
        const auto filterKeyIds = passFilter->filters();
        for (const QNodeId &filterKeyId : filterKeyIds) {
            foundMatch = false;
            Q_ASSERT(filterKeys.contains(filterKeyId));
            const auto &filterFilterKey = filterKeys[filterKeyId];

            const auto passFilterKeyIds = renderPass->filterKeys();
            for (const QNodeId &passFilterKeyId : passFilterKeyIds) {
                if ((foundMatch = (passFilterKeyId == filterKeyId)))
                    break;
                Q_ASSERT(filterKeys.contains(passFilterKeyId));
                const auto &passFilterKey = filterKeys[passFilterKeyId];
                if ((foundMatch = (*passFilterKey == *filterFilterKey)))
                    break;
            }

            if (!foundMatch) {
                // No match for criterion in any of the render pass' criteria
                break;
            }
        }

        if (foundMatch) {
            // Found a renderpass that satisfies our needs. Add it in order
            passes << renderPass;
        }
    }

    return passes;
}

GatheredParameters gatherMaterialParameters(GatheredParameters gatherResult,
                                            const RenderViews &renderViews,
                                            const GraphicsApiFilterData &contextInfo,
                                            const ValueContainer<Parameter> &parameters,
                                            const ValueContainer<Material> &materials,
                                            const ValueContainer<Effect> &effects,
                                            const ValueContainer<Technique> &techniques,
                                            const ValueContainer<RenderPass> &renderPasses,
                                            const ValueContainer<FilterKey> &filterKeys)
{
    gatherResult.reset();

    bool needsRebuild = renderViews.anythingDirty() || materials.anythingDirty()
            || effects.anythingDirty() || techniques.anythingDirty()
            || renderPasses.anythingDirty() || filterKeys.anythingDirty();

    if (!needsRebuild)
        return gatherResult;

    // TODO reorder so that we reuse results from renderViews with the same filters
    // TODO i.e. consider a combinatoric approach to this (filters*materials*techniques*...)
    // TODO note that we may have materials that are not in use by all views, but we still gather them
    // (if entities are filtered out by layers) and we should ideally gather entities * views first,
    // then find the materials that are active, and then gather parameters for each active material
    auto parametersForView = [=](const QNodeId &renderViewId, const Immutable<RenderView> &renderView) {
        Q_UNUSED(renderViewId);
        RenderViewParameterIds renderViewParameterIds;

        if (renderView->noDraw)
            return renderViewParameterIds;

        const auto &techniqueFilter = renderView->techniqueFilter;
        const auto &renderPassFilter = renderView->renderPassFilter;
        for (const auto &material : materials) {
            MaterialParameterIds materialParameterIds;
            if (Q_UNLIKELY(!material->isEnabled()))
                continue;

            Q_ASSERT(effects.contains(material->effect()));
            const auto &effect = effects[material->effect()];

            const auto &technique = findTechniqueForEffect(techniques,
                                                           filterKeys,
                                                           techniqueFilter,
                                                           effect,
                                                           contextInfo);

            const auto &passes = findRenderPassesForTechnique(renderPasses,
                                                              filterKeys,
                                                              renderPassFilter,
                                                              technique);

            if (Q_UNLIKELY(passes.size() == 0))
                continue;
            // Order set:
            // 1 Pass Filter
            // 2 Technique Filter
            // 3 Material
            // 4 Effect
            // 5 Technique
            // 6 RenderPass

            ParameterNameToIdMap result;

            auto update = [parameters, renderView](ParameterNameToIdMap result,
                                                   const QVector<QNodeId> &paramIds) {
                for (const auto &parameterId : paramIds) {
                    const auto &parameter = parameters[parameterId];
                    result[parameter->nameId()] = parameterId;
                }
                return result;
            };

            if (renderView->hasRenderPassFilter)
                result = update(std::move(result), renderView->renderPassFilter->parameters());
            if (renderView->hasTechniqueFilter)
                result = update(std::move(result), renderView->techniqueFilter->parameters());
            // Get the parameters for our selected rendering setup (override what was
            //            defined in the technique/pass filter)
            result = update(std::move(result), material->parameters());
            result = update(std::move(result), effect->parameters());
            result = update(std::move(result), technique->parameters());

            for (const auto &renderPass : passes) {
                if (!renderPass->isEnabled())
                    continue;
                RenderPassParameterIds renderPassParameterIds;
                renderPassParameterIds.renderPass = renderPass;
                // Note: We do not overwrite the result, as each render pass gets different parameters
                renderPassParameterIds.parameterIds = update(result, renderPass->parameters());

                // Note: The actual parameter value will be set below, because this function
                // can be cached as long as only the parameter values have changed
                materialParameterIds.renderPassParameterIds[renderPass->peerId()] = renderPassParameterIds;
            }
            renderViewParameterIds.materialParameterIds[material->peerId()] = materialParameterIds;
        }
        return renderViewParameterIds;
    };

    gatherResult= rebuildAll(std::move(gatherResult), renderViews, parametersForView);

    return gatherResult;
}

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE
