/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT3DRENDER_DRAGON_TEXTURE_H
#define QT3DRENDER_DRAGON_TEXTURE_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "dragonbackendnode_p.h"

#include <private/dragonimmutable_p.h>
#include <private/dragontextureproperties_p.h>

#include <Qt3DRender/qabstracttexture.h>
#include <Qt3DRender/qtexturedata.h>
#include <Qt3DRender/qtexturewrapmode.h>

#include <private/q3dsruntimeglobal_p.h>

#include <private/dragonimmutable_p.h>

#include <QFuture>

QT_BEGIN_NAMESPACE
namespace Qt3DRender {
namespace Dragon {

struct LoadedTextureImage;

class Q3DSV_PRIVATE_EXPORT Texture : public BackendNode
{
public:
    enum DirtyFlag {
        NotDirty = 0,
        DirtyProperties = 0x1,
        DirtyParameters = 0x2,
        DirtyImageGenerators = 0x4,
        DirtyDataGenerator = 0x8
    };
    Q_DECLARE_FLAGS(DirtyFlags, DirtyFlag)

    virtual ~Texture() = default;

    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e) override;
    void initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change) final;
    void addDirtyFlag(DirtyFlags flags);

    DirtyFlags dirty = NotDirty;
    TextureProperties properties;
    TextureParameters parameters;

    QTextureGeneratorPtr generator;

    // TODO this is not used because a texture image must be a child of a texture to work,
    // due to a limitation in the change arbiter not being able to pick up changes at construction
    // time.
    // Consider fixing the change arbiter by using the new change system.
    //    QVector<Qt3DCore::QNodeId> m_textureImages;
};

bool operator ==(const Texture &a, const Texture &b);

struct LoadedTexture
{
    Immutable<Texture> texture;
    QVector<Immutable<LoadedTextureImage>> images;
    QTextureDataPtr data;
};

bool operator ==(const LoadedTexture &a, const LoadedTexture &b);;

} // namespace Dragon
} // namespace Qt3DRender
QT_END_NAMESPACE

#endif // QT3DRENDER_DRAGON_TEXTURE_H
