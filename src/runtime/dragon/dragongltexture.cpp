/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragongltexture_p.h"

#include <private/dragontextureimage_p.h>
#include <private/dragontexture_p.h>

#include <QOpenGLContext>
#include <QOpenGLPixelTransferOptions>

QT_BEGIN_NAMESPACE

using namespace Qt3DCore;

namespace Qt3DRender {
namespace Dragon {

namespace {

void uploadGLData(QOpenGLTexture *glTex,
                  int level,
                  int layer,
                  QOpenGLTexture::CubeMapFace face,
                  const QByteArray &bytes,
                  const QTextureImageDataPtr &data)
{
    if (data->isCompressed()) {
        glTex->setCompressedData(level, layer, face, bytes.size(), bytes.constData());
    } else {
        QOpenGLPixelTransferOptions uploadOptions;
        uploadOptions.setAlignment(1);
        glTex->setData(level, layer, face, data->pixelFormat(), data->pixelType(),
                       bytes.constData(), &uploadOptions);
    }
}
}

TextureProperties deriveProperties(const Immutable<LoadedTexture> &loadedTexture)
{
    TextureProperties properties;
    properties = loadedTexture->texture->properties;

    if (loadedTexture->texture->generator != nullptr) {
        // TODO untested branch, needs testing
        if (loadedTexture->data == nullptr) {
            qWarning() << "[Qt3DRender::GLTexture] No QTextureData generated from Texture "
                          "Generator yet. Texture will be invalid for this frame";
        }

        properties.width = loadedTexture->data->width();
        properties.height = loadedTexture->data->height();
        properties.depth = loadedTexture->data->depth();
        properties.layers = loadedTexture->data->layers();
        properties.format = loadedTexture->data->format();

        const QVector<QTextureImageDataPtr> imageData = loadedTexture->data->imageData();

        if (imageData.size() > 0) {
            // Set the mips level based on the first image if autoMipMapGeneration is disabled
            if (!properties.generateMipMaps)
                properties.mipLevels = imageData.first()->mipLevels();
        }
    }

    // TODO should this be an else if? Why would we set differing sizes on images and generator?
    if (loadedTexture->images.size() > 0) {
        const auto &images = loadedTexture->images;
        int mipLevels = 1;
        for (const auto &loadedImage : images) {
            const auto &image = loadedImage->image;
            const auto imageData = loadedImage->data;
            if (imageData == nullptr) {
                qWarning() << "WARNING: Not finished loading, nothing we can do...";
                continue;
            }
            // If the texture doesn't have a texture generator, we will
            // derive some properties from the first TextureImage (layer=0, miplvl=0, face=0)
            if (!loadedTexture->data && image->layer == 0 && image->mipLevel == 0
                && image->face == QAbstractTexture::CubeMapPositiveX) {
                if (imageData->width() != -1 && imageData->height() != -1
                    && imageData->depth() != -1) {
                    properties.width = imageData->width();
                    properties.height = imageData->height();
                    properties.depth = imageData->depth();
                }
                // Set the format of the texture if the texture format is set to Automatic
                if (properties.format == QAbstractTexture::Automatic) {
                    properties.format = static_cast<QAbstractTexture::TextureFormat>(
                            imageData->format());
                }
            }
            mipLevels = qMax(mipLevels, image->mipLevel);
        }
        properties.mipLevels = mipLevels;
    }
    return properties;
}

GLTexture createGlTexture(const Immutable<LoadedTexture> &loadedTexture, QOpenGLContext *openGLContext)
{
    Q_ASSERT(openGLContext != nullptr);

    bool needUpload = false;
    if (loadedTexture->texture->generator != nullptr || loadedTexture->images.size() > 0) {
        needUpload = true;
    }

    GLTexture result;
    result.loadedTexture = loadedTexture;
    result.properties = deriveProperties(loadedTexture);
    result.parameters = loadedTexture->texture->parameters;
    result.actualTarget = loadedTexture->texture->properties.target;

    if (loadedTexture->texture->generator) {
        result.actualTarget = loadedTexture->data->target();
    }

    // don't try to create the texture if the format was not set
    if (result.properties.format == QAbstractTexture::Automatic) {
        qWarning() << Q_FUNC_INFO << "WARNING: Automatic format";
        return GLTexture();
    }

    if (result.actualTarget == QAbstractTexture::TargetAutomatic) {
        // TODO comment below is before Dragon - might not make any sense anymore
        // If the target is automatic at this point, it means that the texture
        // hasn't been loaded yet (case of remote urls) and that loading failed
        // and that target format couldn't be deduced
        qWarning() << Q_FUNC_INFO << "WARNING: Target automatic";
        return GLTexture();
    }

    QOpenGLTexture *glTex = new QOpenGLTexture(static_cast<QOpenGLTexture::Target>(result.actualTarget));

    // result.format may not be ES2 compatible. Now it's time to convert it, if necessary.
    QAbstractTexture::TextureFormat format = result.properties.format;
    if (openGLContext->isOpenGLES() && openGLContext->format().majorVersion() < 3) {
        switch (result.properties.format) {
        case QOpenGLTexture::RGBA8_UNorm:
        case QOpenGLTexture::RGBAFormat:
            format = QAbstractTexture::RGBAFormat;
            break;
        case QOpenGLTexture::RGB8_UNorm:
        case QOpenGLTexture::RGBFormat:
            format = QAbstractTexture::RGBFormat;
            break;
        case QOpenGLTexture::DepthFormat:
            format = QAbstractTexture::DepthFormat;
            break;
        default:
            qWarning() << Q_FUNC_INFO
                       << "could not find a matching OpenGL ES 2.0 unsized texture format";
            break;
        }
    }

    // Map ETC1 to ETC2 when supported. This allows using features like
    // immutable storage as ETC2 is standard in GLES 3.0, while the ETC1 extension
    // is written against GLES 1.0.
    if (result.properties.format == QAbstractTexture::RGB8_ETC1) {
        if ((openGLContext->isOpenGLES() && openGLContext->format().majorVersion() >= 3)
            || openGLContext->hasExtension(QByteArrayLiteral("GL_OES_compressed_ETC2_RGB8_texture"))
            || openGLContext->hasExtension(QByteArrayLiteral("GL_ARB_ES3_compatibility")))
            format = result.properties.format = QAbstractTexture::RGB8_ETC2;
    }

    glTex->setFormat(result.properties.format == QAbstractTexture::Automatic
                             ? QOpenGLTexture::NoFormat
                             : static_cast<QOpenGLTexture::TextureFormat>(format));
    glTex->setSize(result.properties.width, result.properties.height, result.properties.depth);
    // Set layers count if texture array
    if (result.actualTarget == QAbstractTexture::Target1DArray
        || result.actualTarget == QAbstractTexture::Target2DArray
        || result.actualTarget == QAbstractTexture::Target3D
        || result.actualTarget == QAbstractTexture::Target2DMultisampleArray
        || result.actualTarget == QAbstractTexture::TargetCubeMapArray) {
        glTex->setLayers(result.properties.layers);
    }

    if (result.actualTarget == QAbstractTexture::Target2DMultisample
        || result.actualTarget == QAbstractTexture::Target2DMultisampleArray) {
        // Set samples count if multisampled texture
        // (multisampled textures don't have mipmaps)
        glTex->setSamples(result.properties.samples);
    } else if (result.properties.generateMipMaps) {
        glTex->setMipLevels(glTex->maximumMipLevels());
    } else {
        glTex->setAutoMipMapGenerationEnabled(false);
        if (glTex->hasFeature(QOpenGLTexture::TextureMipMapLevel)) {
            glTex->setMipBaseLevel(0);
            glTex->setMipMaxLevel(result.properties.mipLevels - 1);
        }
        glTex->setMipLevels(result.properties.mipLevels);
    }

    if (!glTex->create()) {
        qWarning() << Q_FUNC_INFO << "creating QOpenGLTexture failed";
        return GLTexture();
    }

    glTex->allocateStorage();
    if (!glTex->isStorageAllocated()) {
        qWarning() << Q_FUNC_INFO << "Storage not allocated";
        return GLTexture();
    }

    result.openGLTexture.reset(glTex);

    if (!needUpload) {
        return result;
    }

    result = reloadGLTexture(std::move(result), loadedTexture);

    //    if (testDirtyFlag(Properties) || testDirtyFlag(Parameters)) {
    glTex->setWrapMode(QOpenGLTexture::DirectionS,
                       static_cast<QOpenGLTexture::WrapMode>(result.parameters.wrapModeX));
    if (result.actualTarget != QAbstractTexture::Target1D
        && result.actualTarget != QAbstractTexture::Target1DArray
        && result.actualTarget != QAbstractTexture::TargetBuffer)
        glTex->setWrapMode(QOpenGLTexture::DirectionT,
                           static_cast<QOpenGLTexture::WrapMode>(result.parameters.wrapModeY));
    if (result.actualTarget == QAbstractTexture::Target3D)
        glTex->setWrapMode(QOpenGLTexture::DirectionR,
                           static_cast<QOpenGLTexture::WrapMode>(result.parameters.wrapModeZ));
    glTex->setMinMagFilters(static_cast<QOpenGLTexture::Filter>(
                                    result.parameters.minificationFilter),
                            static_cast<QOpenGLTexture::Filter>(
                                    result.parameters.magnificationFilter));
    if (glTex->hasFeature(QOpenGLTexture::AnisotropicFiltering))
        glTex->setMaximumAnisotropy(result.parameters.maximumAnisotropy);
    if (glTex->hasFeature(QOpenGLTexture::TextureComparisonOperators)) {
        glTex->setComparisonFunction(
                static_cast<QOpenGLTexture::ComparisonFunction>(result.parameters.comparisonFunction));
        glTex->setComparisonMode(
                static_cast<QOpenGLTexture::ComparisonMode>(result.parameters.comparisonMode));
    }

    return result;
}

// TODO make not take GLTexture by reference
GLTexture reloadGLTexture(GLTexture tmp, const Immutable<LoadedTexture> &loadedTexture)
{
    if (loadedTexture->data) {
        const QVector<QTextureImageDataPtr> imgData = loadedTexture->data->imageData();

        for (const QTextureImageDataPtr &data : imgData) {
            const int mipLevels = tmp.properties.generateMipMaps ? 1 : data->mipLevels();

            for (int layer = 0; layer < data->layers(); layer++) {
                for (int face = 0; face < data->faces(); face++) {
                    for (int level = 0; level < mipLevels; level++) {
                        const QByteArray &bytes = data->data(layer, face, level);
                        uploadGLData(tmp.openGLTexture.get(),
                                     level,
                                     layer,
                                     static_cast<QOpenGLTexture::CubeMapFace>(
                                             QOpenGLTexture::CubeMapPositiveX + face),
                                     bytes,
                                     data);
                    }
                }
            }
        }
    }

    // Upload all QTexImageData references by the TextureImages
    const auto &images = loadedTexture->images;
    for (const auto &loadedImage : images) {
        const auto &image = loadedImage->image;
        const auto imageData = loadedImage->data;

        const QByteArray &bytes = imageData->data();
        uploadGLData(tmp.openGLTexture.get(),
                     image->mipLevel,
                     image->layer,
                     static_cast<QOpenGLTexture::CubeMapFace>(image->face),
                     bytes,
                     imageData);
    }
    return tmp;
}

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE
