/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DRAGONGLBUFFER_P_H
#define DRAGONGLBUFFER_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <QOpenGLContext>
#include <QOpenGLFunctions>

#include <private/dragonbuffer_p.h>
#include <private/dragonimmutable_p.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

class ActivatedSurface;

struct GLBuffer
{
    enum Type {
        ArrayBuffer = 0,
        UniformBuffer,
        IndexBuffer,
        ShaderStorageBuffer,
        PixelPackBuffer,
        PixelUnpackBuffer,
        DrawIndirectBuffer
    };

    GLBuffer() = default;
    GLBuffer(const GLBuffer &other) = delete;
    GLBuffer(GLBuffer &&other);
    GLBuffer(const ActivatedSurface &surface, const Immutable<LoadedBuffer> &buffer);
    GLBuffer &operator=(const GLBuffer &other) = delete;
    GLBuffer &operator=(GLBuffer &&other);
    ~GLBuffer();

    GLuint bufferId() const;

    void reload(const Immutable<LoadedBuffer> &loadedBuffer);

private:
    GLuint m_bufferId = 0;
    QOpenGLContext *m_glContext = nullptr;
    Immutable<LoadedBuffer> m_loadedBuffer;
};

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // DRAGONGLBUFFER_P_H
