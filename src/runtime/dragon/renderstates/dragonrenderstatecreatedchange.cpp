/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "dragonrenderstatecreatedchange_p.h"

#include <Qt3DCore/private/qnodecreatedchange_p.h>

#include <Qt3DRender/QRenderState>

#include <Qt3DRender/private/qrenderstate_p.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {

namespace Dragon {

class RenderStateCreatedChangeBasePrivate : public Qt3DCore::QNodeCreatedChangeBasePrivate
{
public:
    RenderStateCreatedChangeBasePrivate(const Qt3DRender::QRenderState *renderState)
        : Qt3DCore::QNodeCreatedChangeBasePrivate(renderState)
        , m_type(static_cast<Dragon::StateMask>(QRenderStatePrivate::get(renderState)->m_type))
    {
    }

    Dragon::StateMask m_type;
};

RenderStateCreatedChangeBase::RenderStateCreatedChangeBase(const Qt3DRender::QRenderState *renderState)
    : QNodeCreatedChangeBase(*new RenderStateCreatedChangeBasePrivate(renderState), renderState)
{
}

/*
 * \return the current render state type
 */
Dragon::StateMask RenderStateCreatedChangeBase::renderStateType() const
{
    Q_D(const RenderStateCreatedChangeBase);
    return d->m_type;
}

QT_END_NAMESPACE

} // namespace Dragon

} // namespace Qt3DRender
