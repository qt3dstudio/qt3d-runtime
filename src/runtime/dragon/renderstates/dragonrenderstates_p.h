/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QT3DRENDER_DRAGON_RENDERSTATES_H
#define QT3DRENDER_DRAGON_RENDERSTATES_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/dragongenericstate_p.h>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

class BlendEquationArguments : public GenericState<BlendEquationArguments, BlendEquationArgumentsMask, GLenum, GLenum, GLenum, GLenum, bool, int>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class BlendEquation : public GenericState<BlendEquation, BlendStateMask, GLenum>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class AlphaFunc : public GenericState<AlphaFunc, AlphaTestMask, GLenum, GLclampf>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class MSAAEnabled : public GenericState<MSAAEnabled, MSAAEnabledStateMask, GLboolean>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class DepthTest : public GenericState<DepthTest, DepthTestStateMask, GLenum>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class NoDepthMask : public GenericState<NoDepthMask, DepthWriteStateMask, GLboolean>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class CullFace : public GenericState<CullFace, CullFaceStateMask, GLenum>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class FrontFace : public GenericState<FrontFace, FrontFaceStateMask, GLenum>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class Dithering : public GenericState<Dithering, DitheringStateMask>
{
};

class ScissorTest : public GenericState<ScissorTest, ScissorStateMask, int, int, int, int>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class StencilTest : public GenericState<StencilTest, StencilTestStateMask, GLenum, int, uint, GLenum, int, uint>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class AlphaCoverage : public GenericState<AlphaCoverage, AlphaCoverageStateMask>
{
};

class PointSize : public GenericState<PointSize, PointSizeMask, bool, GLfloat>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class PolygonOffset : public GenericState<PolygonOffset, PolygonOffsetStateMask, GLfloat, GLfloat>
{
public:

    void updateProperty(const char *name, const QVariant &value) override;
};

class ColorMask : public GenericState<ColorMask, ColorStateMask, GLboolean, GLboolean, GLboolean, GLboolean>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class ClipPlane : public GenericState<ClipPlane, ClipPlaneMask, int, QVector3D, float>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class SeamlessCubemap : public GenericState<SeamlessCubemap, SeamlessCubemapMask>
{
};

class StencilOp : public GenericState<StencilOp, StencilOpMask, GLenum, GLenum, GLenum, GLenum, GLenum, GLenum>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class StencilMask : public GenericState<StencilMask, StencilWriteStateMask, uint, uint>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

class LineWidth : public GenericState<LineWidth, LineWidthMask, GLfloat, bool>
{
public:
    void updateProperty(const char *name, const QVariant &value) override;
};

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // QT3DRENDER_DRAGON_RENDERSTATES_H
