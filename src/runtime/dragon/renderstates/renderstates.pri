INCLUDEPATH += $$PWD

SOURCES += \
    $$PWD/dragonrenderstateset.cpp \
    $$PWD/dragonrenderstatenode.cpp \
    $$PWD/dragonrenderstates.cpp \
    $$PWD/dragonstatevariant.cpp \
    $$PWD/dragonrenderstatecreatedchange.cpp

HEADERS += \
    $$PWD/dragonrenderstateset_p.h \
    $$PWD/dragonrenderstatenode_p.h \
    $$PWD/dragonrenderstates_p.h \
    $$PWD/dragonstatemask_p.h \
    $$PWD/dragonstatevariant_p.h \
    $$PWD/dragongenericstate_p.h \
    $$PWD/dragonrenderstatecreatedchange_p.h
