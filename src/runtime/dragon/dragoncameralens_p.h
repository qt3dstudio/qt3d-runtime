/****************************************************************************
**
** Copyright (C) 2015 Klaralvdalens Datakonsult AB (KDAB).
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DRAGONCAMERALENS_P_H
#define DRAGONCAMERALENS_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of other Qt classes.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include <private/matrix4x4_p.h>
#include <private/dragonbackendnode_p.h>
#include <private/qbuffer_p.h>

#include <Qt3DRender/qbuffer.h>
#include <Qt3DRender/qbufferdatagenerator.h>

#include <QFuture>

QT_BEGIN_NAMESPACE

namespace Qt3DRender {
namespace Dragon {

class CameraLens : public BackendNode
{
public:
    Matrix4x4 projection() const;
    float exposure() const;

    Matrix4x4 projectionMatrix() const;

    void initializeFromPeer(const Qt3DCore::QNodeCreatedChangeBasePtr &change);
    void sceneChangeEvent(const Qt3DCore::QSceneChangePtr &e);
    Matrix4x4 viewMatrix(const Matrix4x4 &worldTransform) const;
private:

    Matrix4x4 m_projectionMatrix;
    float m_exposure = 0.0f;
};

} // namespace Dragon
} // namespace Qt3DRender

QT_END_NAMESPACE

#endif // DRAGONCAMERALENS_P_H
