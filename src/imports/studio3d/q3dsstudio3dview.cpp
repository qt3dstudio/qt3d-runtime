/****************************************************************************
**
** Copyright (C) 2018 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "q3dsstudio3dview_p.h"
#include "q3dsstudio3ditem_p.h"
#include <private/q3dslayer3dsgnode_p.h>
#include <private/q3dsengine_p.h>
#include <QImage>
#include <QQuickWindow>
#include <QSGTexture>
#include <QSGTextureProvider>

QT_BEGIN_NAMESPACE

/*!
    \qmltype View3D
    \instantiates Q3DSStudio3DView
    \inqmlmodule QtStudio3D
    \ingroup 3dstudioruntime2
    \inherits Item
    \keyword View3D
    \since Qt 3D Studio 2.2

    \brief An item rendering the contents of a single layer from a Qt 3D Studio presentation.

    View3D items display the contents of a single Qt 3D Studio layer. This
    allows taking full control over layer composition and moving it into the
    domain of Qt Quick and QML.

    When there is at least one View3D in the scene, the \l Studio3D item will
    no longer display any content, and its size is not relevant anymore. The
    final composition by the Qt 3D Studio engine is disabled in the mode since
    composition is done by the means of Qt Quick, by positioning, sizing and
    blending the View3D items.

    \note View3D can improve performance significantly (the gain depends on the
    size of the item) for Qt 3D Studio presentations with a single layer in the
    scene. This is because the unnecessary - but with \l Studio3D implicit -
    composition step (rendering the layer into a texture and then blending it
    with the output from other layers) is avoided. This can be especially
    important on devices with mobile/embedded GPUs that have more limited
    fragment processing power.

    \note Views are also useful to avoid the resource and performance
    implications of having multiple \l Studio3D items in a scene: Instead of
    multiple \l Studio3D items (and thus running multiple instances of 3D
    engines in parallel), have only one presentation, represented by a
    \l Studio3D item, and display and compose the contents of the layers via
    separate View3D items. This way there is only one 3D engine running under
    the hood, taking care of the contents of all the View3D items.

    \section2 Example usage

    Let's assume \c{presentation.uip} has two layers, named \c LayerA and
    \c LayerB, with LayerA being on top.

    \image twolayers.png

    The following snippet defines a Studio3D item that displays the composed
    contents of the two layers inside its bounds:

    \qml
    Studio3D {
        anchors.fill: parent
        Presentation {
            source: "qrc:///presentation.uip"
        }
    }
    \endqml

    The following changes to the separate engine - views model. The Studio3D
    item no longer displays anything. It is now up to the application to
    control the position and other settings of the View3D items:

    \qml
    Studio3D {
        id: s3d
        Presentation {
            source: "qrc:///presentation.uip"
        }
    }

    View3D {
        engine: s3d
        source: "LayerB"
        anchors.fill: parent

        View3D {
            engine: s3d
            source: "LayerA"
            anchors.fill: parent
        }
    }
    \endqml

    Under the hood the Qt 3D Studio presentation is constructed as usual, with
    one OpenGL texture for each layer. This also means that when multiple
    View3D items refer to the same layer, they reuse the same texture and the
    content of each layer is rendered only once.

    \section2 Interaction with additional features

    The debug and profiling views, that are normally rendered on top of the 3D
    content, are managed differently when View3D instances are present. Instead
    of using ViewerSettings::showRenderStats or Presentation::profileUiVisible,
    applications are expected to add a \l Studio3DProfiler item to their Qt
    Quick scene. When this item is made visible, the debug and profiling views
    will appear. In most cases the item will be on top everything else, filling
    the viewport:

    \qml
    Studio3DProfiler {
        anchors.fill: parent
        visible: true
    }
    \endqml

    Adding this snippet to the above example would make the application start
    with the debug views shown.

    Check out the \l{Qt 3D Studio Runtime: Layers in Qt Quick Example}{Layers
    in Qt Quick example} for a fully featured example application.
 */

class Q3DSStudio3DViewTextureProvider : public QSGTextureProvider
{
public:
    QSGTexture *texture() const override { return m_texture; }
    void setTexture(QSGTexture *t) {
        if (t != m_texture) {
            m_texture = t;
            emit textureChanged();
        }
    }
private:
    QSGTexture *m_texture = nullptr;
};

Q3DSStudio3DView::Q3DSStudio3DView(QQuickItem *parent)
    : QQuickItem(parent)
{
    setFlag(QQuickItem::ItemHasContents, true);

    setAcceptedMouseButtons(Qt::MouseButtonMask);

    // Do not opt in for hover events - with separate views the profileui is
    // not handled via Qt 3D and instead it is using its own real QQuickItem,
    // taking care of input on its own. Nothing in the 3D scene needs hover
    // (move events when no button is down) so do not waste time with it.
}

Q3DSStudio3DView::~Q3DSStudio3DView()
{
    if (m_engine)
        m_engine->unregisterView(this);

    delete m_textureProvider;
}

// Relying on updatePaintNode to keep the SG node up-to-date is not feasable
// since the Qt3D rendering of the frame - and so the retrieval of the texture
// IDs - is happening in beforeRendering, which happens after the sync step of
// the Quick scenegraph. From there we cannot dirty the item and it's too late
// anyway. Instead, the SG node has to be updated directly from
// Q3DSStudio3DRenderer.

QSGNode *Q3DSStudio3DView::updatePaintNode(QSGNode *node, QQuickItem::UpdatePaintNodeData *)
{
    Q3DSLayer3DSGNode *n = static_cast<Q3DSLayer3DSGNode *>(node);

    if (m_resetToDummy && n && n->texture() == m_dummyTexture)
        m_resetToDummy = false;

    if (!m_dummyTexture || m_resetToDummy) {
        QImage img(64, 64, QImage::Format_ARGB32_Premultiplied);
        img.fill(Qt::transparent);
        m_dummyTexture = window()->createTextureFromImage(img);
    }

    if (!n) {
        n = new Q3DSLayer3DSGNode;
        n->setRect(QRectF(0, 0, 64, 64));
        n->setTexture(m_dummyTexture);
        n->setOwnsTexture(true);
    }

    m_node = n;

    if (m_resetToDummy) {
        m_resetToDummy = false;
        n->setTexture(m_dummyTexture);
    }

    return n;
}

void Q3DSStudio3DView::itemChange(QQuickItem::ItemChange change,
                                  const QQuickItem::ItemChangeData &changeData)
{
    if (change == QQuickItem::ItemSceneChange) {
        if (changeData.window && m_engine) {
            const QSize sz = size().toSize() * changeData.window->effectiveDevicePixelRatio();
            m_engine->handleViewGeometryChange(this, sz);
        }
    }
}

void Q3DSStudio3DView::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickItem::geometryChanged(newGeometry, oldGeometry);

    if (!newGeometry.isEmpty() && m_engine && newGeometry.size() != oldGeometry.size()) {
        const QSize sz = newGeometry.size().toSize() * window()->effectiveDevicePixelRatio();
        m_engine->handleViewGeometryChange(this, sz);
    }
}

/*!
    \qmlproperty Object View3D::engine

    Reference to a \l Studio3D item. This must always be set in order for the
    View3D to display something.
*/

QObject *Q3DSStudio3DView::engine() const
{
    return m_engine;
}

void Q3DSStudio3DView::setEngine(QObject *e)
{
    Q3DSStudio3DItem *item = qobject_cast<Q3DSStudio3DItem *>(e);
    if (!item) {
        qWarning() << e << "is not a Studio3D";
        return;
    }

    if (m_engine == item)
        return;

    if (m_engine)
        m_engine->unregisterView(this);

    m_engine = item;

    if (m_engine) {
        m_engine->registerView(this);
        if (window()) {
            const QSize sz = size().toSize() * window()->effectiveDevicePixelRatio();
            m_engine->handleViewGeometryChange(this, sz);
        }
    }

    connect(m_engine, &QObject::destroyed, this, [this](QObject *obj) {
        if (obj == m_engine)
            m_engine = nullptr;
    });

    emit engineChanged();
    if (window())
        window()->update();
}

/*!
    \qmlproperty string View3D::source

    Specifies a layer in the \c{.uip} presentation that is loaded by associated
    the \l Studio3D item. This must always be set in order for the View3D to
    display something.

    The syntax of the value is the same as in Q3DSPresentation::setAttribute().
    In most cases this will be a single name that is specified in the Qt 3D
    Studio application by renaming the Layer node in the Timeline view.
*/

QString Q3DSStudio3DView::source() const
{
    return m_source;
}

void Q3DSStudio3DView::setSource(const QString &s)
{
    if (m_source == s)
        return;

    m_source = s;

    if (m_engine)
        m_engine->handleViewSourceChange(this);

    emit sourceChanged();
    if (window())
        window()->update();
}

bool Q3DSStudio3DView::sizeLayerToView() const
{
    return m_sizeLayerToView;
}

void Q3DSStudio3DView::setSizeLayerToView(bool v)
{
    if (m_sizeLayerToView == v)
        return;

    m_sizeLayerToView = v;

    if (m_engine && window()) {
        const QSize sz = size().toSize() * window()->effectiveDevicePixelRatio();
        m_engine->handleViewGeometryChange(this, sz);
    }

    emit sizeLayerToViewChanged();
}

bool Q3DSStudio3DView::isTextureProvider() const
{
    return QQuickItem::isTextureProvider() || m_canProvideTexture;
}

QSGTextureProvider *Q3DSStudio3DView::textureProvider() const
{
    // render thread

    if (QQuickItem::isTextureProvider())
        return QQuickItem::textureProvider();

    Q_ASSERT(m_canProvideTexture);

    if (!m_textureProvider) {
        m_textureProvider = new Q3DSStudio3DViewTextureProvider;
        m_textureProvider->setTexture(m_lastTexture);
    }

    return m_textureProvider;
}

void Q3DSStudio3DView::notifyTextureChange(QSGTexture *t, int msaaSampleCount)
{
    // render thread

    if (msaaSampleCount > 1) {
        // the scenegraph itself has no support for multisample textures
        m_canProvideTexture = false;
        return;
    }

    if (m_textureProvider)
        m_textureProvider->setTexture(t);

    m_lastTexture = t;
    m_canProvideTexture = true;
}

void Q3DSStudio3DView::notifyLayerNodeChange(Q3DSLayerNode *layer3DS)
{
    // render thread

    m_layer3DS = layer3DS;
}

void Q3DSStudio3DView::resetToDummy()
{
    m_resetToDummy = true;
}

void Q3DSStudio3DView::keyPressEvent(QKeyEvent *event)
{
    if (m_engine && m_engine->engine())
        m_engine->engine()->handleKeyPressEvent(event);
}

void Q3DSStudio3DView::keyReleaseEvent(QKeyEvent *event)
{
    if (m_engine && m_engine->engine())
        m_engine->engine()->handleKeyReleaseEvent(event);
}

void Q3DSStudio3DView::mousePressEvent(QMouseEvent *event)
{
    if (m_engine && m_engine->engine() && m_layer3DS)
        m_engine->engine()->handleMousePressEvent(event, m_layer3DS);
}

void Q3DSStudio3DView::mouseMoveEvent(QMouseEvent *event)
{
    if (m_engine && m_engine->engine() && m_layer3DS)
        m_engine->engine()->handleMouseMoveEvent(event, m_layer3DS);
}

void Q3DSStudio3DView::mouseReleaseEvent(QMouseEvent *event)
{
    if (m_engine && m_engine->engine() && m_layer3DS)
        m_engine->engine()->handleMouseReleaseEvent(event, m_layer3DS);
}

void Q3DSStudio3DView::mouseDoubleClickEvent(QMouseEvent *event)
{
    if (m_engine && m_engine->engine() && m_layer3DS)
        m_engine->engine()->handleMouseDoubleClickEvent(event, m_layer3DS);
}

#if QT_CONFIG(wheelevent)
void Q3DSStudio3DView::wheelEvent(QWheelEvent *event)
{
    if (m_engine && m_engine->engine() && m_layer3DS)
        m_engine->engine()->handleWheelEvent(event, m_layer3DS);
}
#endif

void Q3DSStudio3DView::touchEvent(QTouchEvent *event)
{
    if (m_engine && m_engine->engine() && m_layer3DS)
        m_engine->engine()->handleTouchEvent(event, m_layer3DS);
}

QT_END_NAMESPACE
