TEMPLATE = subdirs

SUBDIRS += \
    simplewindow \
    simpleoffscreen \
    cppdatainput

qtHaveModule(quick) {
    SUBDIRS += simpleqml \
               qmldatainput \
               layersinquick \
               scenemanip \
               stereoscopicqml
}

qtHaveModule(widgets) {
    SUBDIRS += simplewidget
}
