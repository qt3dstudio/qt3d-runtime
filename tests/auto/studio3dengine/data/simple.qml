import QtQuick 2.0
import QtStudio3D 2.1

Rectangle {
    id: root
    color: "lightGray"

    Studio3DEngine {
        id: s3d
    }

    Layer3D {
        engine: s3d
        width: 200
        height: 200

        Group3D {
            Group3D {
            }
        }
    }
}
