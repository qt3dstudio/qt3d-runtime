TEMPLATE = subdirs
SUBDIRS += standalone
qtHaveModule(widgets) {
    SUBDIRS += \
        qt3dsexplorer \
        datamodelgen
}
qtHaveModule(quick) {
    SUBDIRS += \
        qml3dapi \
        pureqml3d \
        manylayersinquick \
        dynamicloadingapp
}
